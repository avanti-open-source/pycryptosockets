FROM python:3.6-alpine

# Copying the repository into the image
COPY . /root/pycryptosockets/

# Token for getting acces private repository
ARG PRIVATE_TOKEN

# Full dependencies of Pypeline and the rest of them
RUN apk add --virtual build-runtime build-base openblas-dev freetype-dev pkgconfig gfortran
RUN ln -s /usr/include/locale.h /usr/include/xlocale.h

RUN apk add --no-cache --virtual linux-headers g++
RUN apk add --no-cache --virtual .build-deps gcc python3-dev alpine-sdk
RUN apk add --no-cache musl-dev libffi-dev openssl-dev

# RUN pip install --no-cache-dir git+https://oauth2:$PRIVATE_TOKEN@gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets.git
RUN pip install --no-cache-dir /root/pycryptosockets/
RUN pip install --no-cache-dir pytest
RUN pip install --no-cache-dir loguru
RUN pip install --no-cache-dir xlrd
