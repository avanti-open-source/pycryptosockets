# Changelog

## Feature
Wednesday 2020, May 13
* **Feature** [Feature add Kraken exchange](https://gitlab.com/avanti-sf/public/avanti-team/proyectos-internos/pycryptosockets/-/merge_requests/31)

## Feature
Thursday 2020, May 12
* **Feature** [Feature add Kraken exchange](https://gitlab.com/avanti-sf/public/avanti-team/proyectos-internos/pycryptosockets/-/merge_requests/31)

## Feature
Tuesday 2020, Apr 29
* **Feature** [Feature add Kraken exchange](https://gitlab.com/avanti-sf/public/avanti-team/proyectos-internos/pycryptosockets/-/merge_requests/30)


## Hotfix
Wednesday 2020, Feb 19
* **Hotfix** [Hotfix for datetime on coinsbit](https://gitlab.com/avanti-sf/public/avanti-team/proyectos-internos/pycryptosockets/-/merge_requests/29)

## Feature
Thursday 2020, Feb 6
* **Change** [add coinsbit](https://gitlab.com/avanti-sf/public/avanti-team/proyectos-internos/pycryptosockets/-/merge_requests/27)

## Feature
Friday 2019, Nov 29
* **Change** [change to url](https://gitlab.com/avanti-sf/public/avanti-team/proyectos-internos/pycryptosockets/merge_requests/25)

## Change
Tuesday 2019, Sep 03
* **Change** [change to url](https://gitlab.com/avanti-sf/public/avanti-team/proyectos-internos/pycryptosockets.git)

## Hotfix
Tuesday 2019, Sep 17
* **Hotfix** [change to match for coinbase && add debug mode for pycrypto](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/23)

## Hotfix
Monday 2019, Sep 16
* **Hotfix** [change to match for coinbase && add debug mode for pycrypto](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/22)

## Feature
Tuesday 2019, Wednesday 11
* **Feature** [CoinBase websocketclient forpycryptosockets](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/19)


## Release 19.16.0
Wednesday 2019, Sep 04

* **Feature** [IDAX websocketclient forpycryptosockets](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/18)
* **Feature** [Bitz websocketclient forpycryptosockets](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/17)
* **Feature** [fatBTC websocketclient forpycryptosockets](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/15)
* **Feature** [Bibox websocketclient forpycryptosockets](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/14)
* **Feature** [CoinTiger websocketclient forpycryptosockets](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/13)
* **kucoin, livecoin, GDAX aren't currently working**


## Feature
Tuesday 2019, Ago 27
* **Feature** [IDAX websocketclient forpycryptosockets](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/18)

## Feature
Monday 2019, Ago 26
* **Feature** [Bitz websocketclient forpycryptosockets](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/17)

## Hotfix
Tuesday 2019, Ago 23
* **Hotfix** [Hotfix for bibox/oex forpycryptosockets](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/16)

## Feature
Wednesday 2019, Ago 22
* **Feature** [fatBTC websocketclient forpycryptosockets](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/15)

## Feature
Wednesday 2019, Ago 21
* **Feature** [Bibox websocketclient forpycryptosockets](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/14)

## Feature
Thursday 2019, Ago 20
* **Feature** [CoinTiger websocketclient forpycryptosockets](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/13)

## Feature
Monday 2019, Ago 19
* **Feature** [CI-CD for pycryptosockets](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/12)

## Feature
Friday 2019, Ago 16

* **Feature** [refactoring for bitbank & dockerfile](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/11')


## Feature
Thursday 2019, Ago 15

* **Feature** [development exchange oex client](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/10')

## Feature
Tuesday 2019, Ago 14

* **Feature** [update for pycryptosockets pytest](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/9)

## Feature
Monday 2019, Ago 12

* **Feature** [code improve for pycryptosockets](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/8)

## Feature
Friday 2019, Ago 09

* **Feature** [update for pycryptosockets exchanges](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/7)

## Feature
Monday 2019, Ago 05

* **Feature** [update for pycryptosockets new repo](https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets/merge_requests/5)