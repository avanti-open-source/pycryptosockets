import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.bitfinex_ws import BitfinexWebsocketClient


class TestBitfinexWebsocketClient:

    bitfinex_client = BitfinexWebsocketClient(reconnect_on_error=False)
    message = '[455280,"te",[386024967,1566265585457,50,0.2791]]'

    def test_connection_client(self):
        self.bitfinex_client.start_connection()
        self.bitfinex_client.close_connection()

    def test_subscribe_channel(self):
        self.bitfinex_client.start_connection()
        self.bitfinex_client.subscribe_socket('BTC-USD')
        self.bitfinex_client.close_connection()

    def test_listen_channel(self):
        self.bitfinex_client.start_connection()
        self.bitfinex_client.subscribe_socket('BTC-USD')
        self.bitfinex_client.listen()
        time.sleep(5)
        self.bitfinex_client.close_connection()

    def test_on_message(self):
        self.bitfinex_client.start_connection()
        self.bitfinex_client.coins_dict = {455280: 'XRPUSD'}
        self.bitfinex_client.coins = {'xrpusd': 'XRP-USD'}
        self.bitfinex_client.on_message(self.message)
        self.bitfinex_client.close_connection()


class TestClientManagerBitfinex:

    bitfinex_manager = ClientsManager(config={'Bitfinex': ['BTC-USD']})

    def test_update_message(self):
        message = dict(event='Update',exchange='Bitfinex',data=dict(symbol='BTC-USD',time='time'))
        self.bitfinex_manager.listening = True
        self.bitfinex_manager.update(message)