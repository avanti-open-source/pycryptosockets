import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.fatbtc_ws import FatBTCWebsocketClient


class TestFatBTCWebsocketClient:

    fatbtc_client = FatBTCWebsocketClient(reconnect_on_error=False)
    fatbtc_client.coins = dict(BTC_USDT='BTC-USDT')
    message = '{"data":{"msg":"success","status":1,"symbol":"btcusdt","timestamp":1566440562916,"trades":[{"price":10011.57,"taker":"buy","timestamp":1566440555553,"upOrDown":1,"volume":0.0075},{"price":9972.43,"taker":"buy","timestamp":1566440539645,"upOrDown":-1,"volume":0.0030},{"price":9975.90,"taker":"sell","timestamp":1566440524717,"upOrDown":-1,"volume":0.0077},{"price":9998.66,"taker":"sell","timestamp":1566440477089,"upOrDown":1,"volume":0.0016},{"price":9972.08,"taker":"buy","timestamp":1566440446220,"upOrDown":-1,"volume":0.0039},{"price":9980.92,"taker":"buy","timestamp":1566440431315,"upOrDown":-1,"volume":0.0055},{"price":9981.76,"taker":"buy","timestamp":1566440417394,"upOrDown":1,"volume":0.0061},{"price":9976.95,"taker":"buy","timestamp":1566440403656,"upOrDown":-1,"volume":0.0080},{"price":10010.17,"taker":"buy","timestamp":1566440389492,"upOrDown":-1,"volume":0.0031},{"price":10014.85,"taker":"sell","timestamp":1566440375495,"upOrDown":-1,"volume":0.0060}]},"key":"trades_btcusdt_10"}'

    @pytest.mark.skip(reason='ws currently is down')
    def test_connection_client(self):
        self.fatbtc_client.start_connection()
        self.fatbtc_client.close_connection()

    @pytest.mark.skip(reason='ws currently is down')
    def test_obtain_timestamp(self):
        data_timestamp = self.fatbtc_client.obtain_timestamp()
        assert isinstance(data_timestamp, str)

    @pytest.mark.skip(reason='ws currently is down')
    def test_subscribe_channel(self):
        self.fatbtc_client.start_connection()
        self.fatbtc_client.subscribe_socket('BTC-USDT')
        time.sleep(2)
        self.fatbtc_client.close_connection()

    @pytest.mark.skip(reason='ws currently is down')
    def test_listen_channel(self):
        self.fatbtc_client.start_connection()
        self.fatbtc_client.subscribe_socket('BTC-USDT')
        self.fatbtc_client.listen()
        time.sleep(2)
        self.fatbtc_client.close_connection()

    @pytest.mark.skip(reason='ws currently is down')
    def test_on_message(self):
        self.fatbtc_client.start_connection()
        self.fatbtc_client.subscribe_socket('BTC-USDT')
        self.fatbtc_client.on_message(self.message)
        self.fatbtc_client.close_connection()


class TestClientManagerFatBTC:

    fatbtc_manager = ClientsManager(config={'FatBTC': ['BTC-USDT']})

    def test_update_message(self):
        message = dict(event='Update',exchange='FatBTC',data=dict(symbol='BTC-USDT',time='time'))
        self.fatbtc_manager.listening = True
        self.fatbtc_manager.update(message)
