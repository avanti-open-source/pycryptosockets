import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.zaif_ws import ZaifWebsocketClient


class TestZaifWebsocketClient:

    zaif_client = ZaifWebsocketClient(reconnect_on_error=False)
    message = '{"asks": [[1157395.0, 0.011], [1157425.0, 0.001], [1157430.0, 0.0368], [1157945.0, 0.002], [1157995.0, 0.003], [1158000.0, 0.0015], [1158020.0, 0.005], [1158500.0, 0.004], [1158590.0, 0.004], [1158695.0, 0.438], [1158700.0, 0.07], [1158990.0, 0.001], [1158995.0, 0.002], [1159000.0, 0.0574], [1159440.0, 0.0128], [1159445.0, 0.002], [1159450.0, 1.369], [1159610.0, 0.0017], [1159625.0, 0.02], [1159660.0, 0.0017]], "last_price": {"action": "bid", "price": 1157485.0}, "target_users": ["b32fdeb0b3", "0acb5b73d0", "9ce9e0d8ff", "9c4130288d"], "trades": [{"currenty_pair": "btc_jpy", "trade_type": "bid", "price": 1157485.0, "currency_pair": "btc_jpy", "date": 1566267837, "amount": 0.005, "tid": 148231288}, {"currenty_pair": "btc_jpy", "trade_type": "ask", "price": 1157500.0, "currency_pair": "btc_jpy", "date": 1566267830, "amount": 0.0005, "tid": 148231287}, {"currenty_pair": "btc_jpy", "trade_type": "ask", "price": 1157500.0, "currency_pair": "btc_jpy", "date": 1566267817, "amount": 0.001, "tid": 148231286}, {"currenty_pair": "btc_jpy", "trade_type": "bid", "price": 1157950.0, "currency_pair": "btc_jpy", "date": 1566267790, "amount": 0.0015, "tid": 148231285}, {"currenty_pair": "btc_jpy", "trade_type": "bid", "price": 1158000.0, "currency_pair": "btc_jpy", "date": 1566267781, "amount": 0.0004, "tid": 148231284}, {"currenty_pair": "btc_jpy", "trade_type": "bid", "price": 1158000.0, "currency_pair": "btc_jpy", "date": 1566267781, "amount": 0.0015, "tid": 148231283}, {"currenty_pair": "btc_jpy", "trade_type": "bid", "price": 1158000.0, "currency_pair": "btc_jpy", "date": 1566267781, "amount": 0.001, "tid": 148231282}, {"currenty_pair": "btc_jpy", "trade_type": "ask", "price": 1157055.0, "currency_pair": "btc_jpy", "date": 1566267779, "amount": 0.03, "tid": 148231281}, {"currenty_pair": "btc_jpy", "trade_type": "ask", "price": 1157055.0, "currency_pair": "btc_jpy", "date": 1566267778, "amount": 0.01, "tid": 148231280}, {"currenty_pair": "btc_jpy", "trade_type": "bid", "price": 1158000.0, "currency_pair": "btc_jpy", "date": 1566267778, "amount": 0.0015, "tid": 148231279}, {"currenty_pair": "btc_jpy", "trade_type": "ask", "price": 1157055.0, "currency_pair": "btc_jpy", "date": 1566267777, "amount": 0.0195, "tid": 148231278}, {"currenty_pair": "btc_jpy", "trade_type": "ask", "price": 1157055.0, "currency_pair": "btc_jpy", "date": 1566267777, "amount": 0.001, "tid": 148231277}, {"currenty_pair": "btc_jpy", "trade_type": "ask", "price": 1157055.0, "currency_pair": "btc_jpy", "date": 1566267777, "amount": 0.001, "tid": 148231276}, {"currenty_pair": "btc_jpy", "trade_type": "ask", "price": 1157060.0, "currency_pair": "btc_jpy", "date": 1566267777, "amount": 0.001, "tid": 148231275}, {"currenty_pair": "btc_jpy", "trade_type": "ask", "price": 1157060.0, "currency_pair": "btc_jpy", "date": 1566267777, "amount": 0.002, "tid": 148231274}, {"currenty_pair": "btc_jpy", "trade_type": "ask", "price": 1157060.0, "currency_pair": "btc_jpy", "date": 1566267777, "amount": 0.001, "tid": 148231273}, {"currenty_pair": "btc_jpy", "trade_type": "ask", "price": 1157060.0, "currency_pair": "btc_jpy", "date": 1566267777, "amount": 0.002, "tid": 148231272}, {"currenty_pair": "btc_jpy", "trade_type": "ask", "price": 1157225.0, "currency_pair": "btc_jpy", "date": 1566267777, "amount": 0.01, "tid": 148231271}, {"currenty_pair": "btc_jpy", "trade_type": "ask", "price": 1157230.0, "currency_pair": "btc_jpy", "date": 1566267777, "amount": 0.001, "tid": 148231270}, {"currenty_pair": "btc_jpy", "trade_type": "ask", "price": 1157230.0, "currency_pair": "btc_jpy", "date": 1566267777, "amount": 0.001, "tid": 148231269}, {"currenty_pair": "btc_jpy", "trade_type": "ask", "price": 1157265.0, "currency_pair": "btc_jpy", "date": 1566267777, "amount": 0.0005, "tid": 148231268}], "bids": [[1157110.0, 0.05], [1157065.0, 0.001], [1157060.0, 0.002], [1157000.0, 0.0069], [1156570.0, 0.003], [1156500.0, 0.0025], [1156055.0, 0.208], [1156050.0, 0.196], [1156045.0, 0.001], [1156035.0, 0.0463], [1156030.0, 0.001], [1156025.0, 0.001], [1156000.0, 0.0834], [1155580.0, 0.0015], [1155500.0, 0.0025], [1155000.0, 0.0479], [1154880.0, 0.1], [1154790.0, 0.036], [1154750.0, 0.002], [1154500.0, 0.0025]], "currency_pair": "btc_jpy", "timestamp": "2019-08-20 11:24:19.426868"}'

    def test_subscribe_channel(self):
        self.zaif_client.subscribe_socket('BTC-JPY')
        time.sleep(3)
        self.zaif_client.close_connection()

    def test_listen_channel(self):
        self.zaif_client.subscribe_socket('BTC-JPY')
        self.zaif_client.listen()
        time.sleep(3)
        self.zaif_client.close_connection()

    def test_on_message(self):
        self.zaif_client.coins = {'btc_jpy': 'BTC-JPY', 'xcp_btc': 'XCP-BTC'}
        self.zaif_client.on_message(self.message)


class TestClientManagerZaif:

    zaif_manager = ClientsManager(config={'Zaif': ['BTC-JPY']})

    def test_update_message(self):
        message = dict(event='Update',exchange='Zaif',data=dict(symbol='BTC-JPY',time='time'))
        self.zaif_manager.listening = True
        self.zaif_manager.update(message)
