import sys
sys.path.insert(0, "..")
from pycryptosockets.datafeeders import zaif_ws as zaif
from pycryptosockets.datafeeders import zaif_ws_test as zw
import requests


client_id = '4221bb3fd7fc4aa3bbee7707319b4e44'
client_secret = '54f32f9feb6d48be9603aabe79021df3'


def zaif_coins():
    url = "https://api.zaif.jp/api/1/currency_pairs/all"
    r = requests.get(url)
    data = r.json()
    pairs = [x['name'].replace("/", "-") for x in data]
    symbols = [x['currency_pair'] for x in data]
    return pairs, symbols


p, s = zaif_coins()
client = zaif.ZaifWebsocketClient(False)
client.start_connection()
client.coins = dict(zip(s, p))
client.subscribe_socket(s[:1])
client.listen()
