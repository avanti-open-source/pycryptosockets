import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.idax_ws import IDAXWebsocketClient


class TestIDAXWebsocketClient:

    idax_client = IDAXWebsocketClient(reconnect_on_error=False)
    idax_message = '{"channel":"idax_sub_btc_usdt_trades","code":"00000","data":[[2611461510000011267,"10149.30","0.693791",1566876908923,"buy"],[2611461510000011273,"10153.60","0.000230",1566876908927,"buy"],[2611461510000011274,"10153.60","0.000933",1566876908927,"buy"]]}'

    def test_connection_client(self):
        self.idax_client.start_connection()
        self.idax_client.close_connection()

    def test_subscribe_channel(self):
        self.idax_client.start_connection()
        self.idax_client.subscribe_socket('BTC-USDT')
        self.idax_client.listen()
        time.sleep(2)
        self.idax_client.close_connection()

    def test_on_message_client(self):
        self.idax_client.start_connection()
        self.idax_client.on_message(self.idax_message)
        self.idax_client.close_connection()


class TestClientManagerIDAX:

    idax_manager = ClientsManager(config={'IDAX': ['BTC-USDT']})

    def test_update_message(self):
        message = dict(event='Update',exchange='IDAX',data=dict(symbol='BTC-USDT',time='time'))
        self.idax_manager.listening = True
        self.idax_manager.update(message)


