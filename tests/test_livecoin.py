import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.livecoin_ws import LivecoinWebsocketClient


class TestLivecoinWebsocketClient:

    live_coin = LivecoinWebsocketClient(reconnect_on_error=False)

    @pytest.mark.skip(reason='ws is fail')
    def test_connection_client(self):
        self.live_coin.start_connection()
        time.sleep(5)
        self.live_coin.close_connection()

    @pytest.mark.skip(reason='ws unable to subscribe')
    def test_subscribe_channel(self):
        self.live_coin.start_connection()
        self.live_coin.subscribe_socket('USD/BTC')
        time.sleep(3)
        self.live_coin.close_connection()

    @pytest.mark.skip(reason='ws unable to subcribe')
    def test_listen_channel(self):
        self.live_coin.start_connection()
        self.live_coin.subscribe_socket('USD/BTC')
        self.live_coin.listen()
        time.sleep(3)
        self.live_coin.close_connection()


class TestClientManagerLivecoin:

    livecoin_manager = ClientsManager(config={'Livecoin': ['USD/BTC']})

    def test_update_message(self):
        message = dict(event='Update',exchange='Livecoin',data=dict(symbol='USD/BTC',time='time'))
        self.livecoin_manager.listening = True
        self.livecoin_manager.update(message)