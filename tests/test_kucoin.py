import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.kucoin_ws import KucoinWebsocketClient


class TestKucoinWebsocketClient:

    kucoin_client = KucoinWebsocketClient(reconnect_on_error=False)

    # @pytest.mark.skip(reason='ws currently is down')
    def test_connection_client(self):
        self.kucoin_client.start_connection()
        self.kucoin_client.close_connection()

    def test_subscribe_channel(self):
        self.kucoin_client.start_connection()
        self.kucoin_client.subscribe_socket('BTC-USDT')
        self.kucoin_client.close_connection()

    def test_listen_channel(self):
        self.kucoin_client.start_connection()
        self.kucoin_client.subscribe_socket('BTC-USDT')
        self.kucoin_client.listen()
        self.kucoin_client.close_connection()


class TestClientManagerKucoin:

    kucoin_manager = ClientsManager(config={'Kucoin': ['BTC-USDT']})

    def test_update_message(self):
        message = dict(event='Update',exchange='Kucoin',data=dict(symbol='BTC-USDT',time='time'))
        self.listening = True
        self.kucoin_manager.listening = True
        self.kucoin_manager.update(message)