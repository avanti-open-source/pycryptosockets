import pytest
import time


from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.bitz_ws import BitzWebsocketClient


class TestBitzWebsocketClient:

    bitz_client = BitzWebsocketClient(reconnect_on_error=False)
    message = '{"msgId":0,"params":{"symbol":"btc_usdt"},"action":"Pushdata.order","data":[{"id":1480828886,"t":"06:30:28","T":1566858628,"p":"10332.08","n":"0.0844","s":"buy"},{"id":1480828654,"t":"06:30:26","T":1566858626,"p":"10331.79","n":"0.0670","s":"buy"}],"time":1566858628988,"source":"sub-api"}'

    def test_connection_client(self):
        self.bitz_client.start_connection()
        self.bitz_client.close_connection()

    def test_subscribe_channel(self):
        self.bitz_client.start_connection()
        self.bitz_client.subscribe_socket('BTC-USDT')
        self.bitz_client.close_connection()

    def test_on_message(self):
        self.bitz_client.start_connection()
        self.bitz_client.on_message(self.message)
        self.bitz_client.close_connection()


class TestClientManagerBitz:

    bitz_manager = ClientsManager(config={'Bitz': ['BTC-USDT']})

    def test_update_message(self):
        message = dict(event='Update',exchange='Bitz',data=dict(symbol='BTC-USDT',time='time'))
        self.bitz_manager.listening = True
        self.bitz_manager.update(message)


