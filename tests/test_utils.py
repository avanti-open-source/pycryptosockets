import pytest

from pycryptosockets.utils.coinsmanager import CoinsManager
from pycryptosockets._enumerates import(
    pycrypto_exchanges,
    needs_auth,
)


class TestCoinsManager(object):

    def test_get_client(self):
        client = CoinsManager()
        client_get = client.get_client('Bitfinex', auth=None, on_error=True)
        check_url = client_get.url
        assert check_url == 'wss://api.bitfinex.com/ws/2'

    def test_validate_pairs(self):
        client = CoinsManager()
        client_get = client.get_client('Bitfinex', auth=None, on_error=True)
        pairs_validate = client.get_validate_pairs('Bitfinex', pairs=['XRP-USD'])
        assert 'xrpusd' in pairs_validate
        assert client_get.url == 'wss://api.bitfinex.com/ws/2'
        


class TestEnumerate(object):
        
    def test_check_exchange_exists_in_list(self):
        exchange = 'Binance'
        exists_exchange = exchange in pycrypto_exchanges
        assert exists_exchange == True

    def test_check_needs_auth_exchange(self):
        exchanges = ['Binance', 'Bitbank']
        exchanges_numbers = 0
        for exchange in exchanges:
            if exchange in needs_auth:
                exchanges_numbers += 1
        assert exchanges_numbers == len(needs_auth)
        