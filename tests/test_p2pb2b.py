import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.p2pb2b_ws import P2PB2BWebsocketClient


class TestP2PB2BWebsocketClient:

    p2pb2b_client = P2PB2BWebsocketClient(reconnect_on_error=False)

    @pytest.mark.skip(reason='ws currently is down')
    def test_connection_client(self):
        self.p2pb2b_client.start_connection()
        self.p2pb2b_client.close_connection()

    @pytest.mark.skip(reason='ws currently is down')
    def test_subscribe_channel(self):
        self.p2pb2b_client.start_connection()
        self.p2pb2b_client.subscribe_socket('BTC-USDT')
        self.p2pb2b_client.close_connection()

    @pytest.mark.skip(reason='ws currently is down')
    def test_listen_channel(self):
        self.p2pb2b_client.start_connection()
        self.p2pb2b_client.subscribe_socket('BTC-USDT')
        self.p2pb2b_client.listen()
        self.p2pb2b_client.close_connection()


class TestClientManagerP2PB2B:

    p2pb2b_manager = ClientsManager(config={'P2PB2B': ['BTC-USDT']})

    def test_update_message(self):
        message = dict(event='Update',exchange='P2PB2B',data=dict(symbol='BTC-USDT',time='time'))
        self.listening = True
        self.p2pb2b_manager.listening = True
        self.p2pb2b_manager.update(message)