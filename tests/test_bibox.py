import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.bibox_ws import BiboxWebsocketClient


class TestBiboxWebsocketClient:

    bibox_client = BiboxWebsocketClient(reconnect_on_error=False)
    bytes_message = '[{"channel":"bibox_sub_spot_BTC_USDT_deals","binary":"1","data_type":1,"data":"H4sIAAAAAAAA/xTIMQ7CMAwF0Lv82aq+28QCj8ANKBNCqIIOHgJRCBPi7oj1nT+oSzQ4dvP+ejoeZghqi9sKh1JHDjalLQRLeb4fHQ4OY9ooSULQo6xwzWZpUuZsaoJX3P/4vfwCAAD///A0TqteAAAA"}]\n'
    ping_message = '{"ping":1566431299}'
    error_message = '{"error": "code-base4"}'
    bibox_client.coins = dict(BTC_USDT='BTC-USDT')

    def test_connection_client(self):
        self.bibox_client.start_connection()
        self.bibox_client.close_connection()

    def test_subscribe_channel(self):
        self.bibox_client.start_connection()
        self.bibox_client.subscribe_socket('BTC-USDT')
        time.sleep(2)
        self.bibox_client.close_connection()

    def test_listen_channel(self):
        self.bibox_client.start_connection()
        self.bibox_client.subscribe_socket('BTC-USDT')
        self.bibox_client.listen()
        time.sleep(2)
        self.bibox_client.close_connection()

    def test_on_message(self):
        self.bibox_client.start_connection()
        self.bibox_client.subscribe_socket('BTC-USDT')
        self.bibox_client.on_message(self.bytes_message)
        self.bibox_client.on_message(self.ping_message)
        self.bibox_client.on_message(self.error_message)
        self.bibox_client.close_connection()


class TestClientManagerBibox:

    bibox_manager = ClientsManager(config={'Bibox': ['BTC-USDT']})

    def test_update_message(self):
        message = dict(event='Update',exchange='Bibox',data=dict(symbol='BTC-USDT',time='time'))
        self.bibox_manager.listening = True
        self.bibox_manager.update(message)