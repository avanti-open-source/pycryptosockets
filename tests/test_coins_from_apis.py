import pytest

from pycryptosockets.datafeeders import _coins


class TestCoins:

    def test_binance_coins(self):
        _, coins = _coins.binance_coins()
        assert len(coins) > 0

    def test_bitbank_coins(self):
        _, coins = _coins.bitbank_coins()
        assert len(coins) > 0

    def test_bitfinex_coins(self):
        _, coins = _coins.bitfinex_coins()
        assert len(coins) > 0

    def test_bitflyer_coins(self):
        _, coins = _coins.bitflyer_coins()
        assert len(coins) > 0

    def test_bitmex_coins(self):
        _, coins = _coins.bitmex_coins()
        assert len(coins) > 0

    def test_bitstamp_coins(self):
        _, coins = _coins.bitstamp_coins()
        assert len(coins) > 0

    def test_bittrex_coins(self):
        _, coins = _coins.bittrex_coins()
        assert len(coins) > 0

    def test_gateio_coins(self):
        _, coins = _coins.gateio_coins()
        assert len(coins) > 0

    def test_hitbtc_coins(self):
        _, coins = _coins.hitbtc_coins()
        assert len(coins) > 0

    def test_huobipro_coins(self):
        _, coins = _coins.huobipro_coins()
        assert len(coins) > 0

    def test_kucoin_coins(self):
        _, coins = _coins.kucoin_coins()
        assert len(coins) > 0

    def test_livecoin_coins(self):
        _, coins = _coins.livecoin_coins()
        assert len(coins) > 0

    def test_okex_coins(self):
        _, coins = _coins.okex_coins()
        assert len(coins) > 0

    def test_poloniex_coins(self):
        _, coins = _coins.poloniex_coins()
        assert len(coins) > 0

    def test_zaif_coins(self):
        _, coins = _coins.zaif_coins()
        assert len(coins) > 0

    def test_bitmax_coins(self):
        _, coins = _coins.bitmax_coins()
        assert len(coins) > 0

    def test_oex_coins(self):
        _, coins = _coins.okex_coins()
        assert len(coins) > 0

    def test_cointiger_coins(self):
        _, coins = _coins.cointiger_coins()
        assert len(coins) > 0

    def test_bitz_coins(self):
        _, coins = _coins.bibox_coins()
        assert len(coins) > 0

    def test_bitz_coins(self):
        _, coins = _coins.bitz_coins()
        assert len(coins) > 0

    def test_idax_coins(self):
        _, coins = _coins.idax_coins()
        assert len(coins) > 0
        assert 'btc_usdt' in coins

    def test_coinbase_coins(self):
        _, coins = _coins.coinbasepro_coins()
        assert len(coins) > 0
        assert 'BTC-USD' in coins

    def test_p2pb2b_coins(self):
        _, coins = _coins.p2pb2b_coins()
        assert len(coins) > 0
        assert 'BTC_USDT' in coins
