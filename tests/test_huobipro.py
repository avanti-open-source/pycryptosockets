import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.huobipro_ws import HuobiproWebsocketClient


class TestHuobiproWebsocketClient:

    huobipro_client = HuobiproWebsocketClient(reconnect_on_error=False)

    def test_connection_client(self):
        self.huobipro_client.start_connection()
        self.huobipro_client.close_connection()

    def test_subscribe_channel(self):
        self.huobipro_client.start_connection()
        self.huobipro_client.subscribe_socket('BTC-USDT')

    def test_listen_channel(self):
        self.huobipro_client.start_connection()
        self.huobipro_client.subscribe_socket('BTC-USDT')
        self.huobipro_client.listen()
        time.sleep(5)
        self.huobipro_client.close_connection()


class TestClientManagerHuobipro:

    huobipro_manager = ClientsManager(config={'Huobipro': ['BTC-USDT']})

    def test_update_message(self):
        message = dict(event='Update',exchange='Huobipro',data=dict(symbol='BTC-USDT',time='time'))
        self.huobipro_manager.listening = True
        self.huobipro_manager.update(message)