import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.bitstamp_ws import BitstampWebsocketClient


class TestBitstampWebsocketClient:

    bitstamp_client = BitstampWebsocketClient(reconnect_on_error=False)

    def test_connection_client(self):
        self.bitstamp_client.start_connection()
        self.bitstamp_client.close_connection()

    def test_subscribe_channel(self):
        self.bitstamp_client.start_connection()
        self.bitstamp_client.subscribe_socket('BTC-USD')

    def test_listen_channel(self):
        self.bitstamp_client.start_connection()
        self.bitstamp_client.subscribe_socket('BTC-USD')
        self.bitstamp_client.listen()
        time.sleep(5)
        self.bitstamp_client.close_connection()


class TestClientManagerBitstamp:

    bitstamp_manager = ClientsManager(config={'Bitstamp': ['BTC-USD']})

    def test_update_message(self):
        message = dict(event='Update',exchange='Bitstamp',data=dict(symbol='BTC-USD',time='time'))
        self.bitstamp_manager.listening = True
        self.bitstamp_manager.update(message)