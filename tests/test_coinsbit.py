import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.coinsbit_ws import CoinsBitWebsocketClient


class TestCoinsBitWebsocketClient:

    coinsbit_client = CoinsBitWebsocketClient(reconnect_on_error=False)

    # @pytest.mark.skip(reason='ws currently is down')
    def test_connection_client(self):
        self.coinsbit_client.start_connection()
        self.coinsbit_client.close_connection()

    # @pytest.mark.skip(reason='ws currently is down')
    def test_subscribe_channel(self):
        self.coinsbit_client.start_connection()
        self.coinsbit_client.subscribe_socket('BTC-USDT')
        self.coinsbit_client.close_connection()

    # @pytest.mark.skip(reason='ws currently is down')
    def test_listen_channel(self):
        self.coinsbit_client.start_connection()
        self.coinsbit_client.subscribe_socket('BTC-USDT')
        self.coinsbit_client.listen()
        self.coinsbit_client.close_connection()


class TestClientManagercoinsbit:

    coinsbit_manager = ClientsManager(config={'coinsbit': ['BTC-USDT']})

    def test_update_message(self):
        message = dict(event='Update',exchange='coinsbit',data=dict(symbol='BTC-USDT',time='time'))
        self.listening = True
        self.coinsbit_manager.listening = True
        self.coinsbit_manager.update(message)