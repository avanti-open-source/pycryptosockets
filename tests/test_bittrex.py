import pytest
import time

from threading import Thread
from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.bittrex_ws import BittrexWebsocketClient


class TestBittrexWebsocketClient:

    bittrex_client = BittrexWebsocketClient(reconnect_on_error=False)

    def test_connection_client(self):
        self.bittrex_client.start_connection()
        time.sleep(5)
        self.bittrex_client.close_connection()


    def test_subscribe_channel(self):
        self.bittrex_client.start_connection()
        self.bittrex_client.subscribe_socket('XRP-BTC')
        time.sleep(5)
        self.bittrex_client.close_connection()


class TestClientManagerBittrex:

    bittrex_manager = ClientsManager(config={'Bittrex': ['XRP-BTC']})

    def test_update_message(self):
        message = dict(event='Update',exchange='Bittrex',data=dict(symbol='XRP-BTC',time='time'))
        self.bittrex_manager.listening = True
        self.bittrex_manager.update(message)