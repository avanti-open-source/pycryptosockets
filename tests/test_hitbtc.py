import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.hitbtc_ws import HitBTCWebsocketClient


class TestHitBTCWebsocketClient:

    hitbtc_client = HitBTCWebsocketClient(reconnect_on_error=False)

    def test_connection_client(self):
        self.hitbtc_client.start_connection()
        self.hitbtc_client.close_connection()
        time.sleep(8)

    def test_subscribe_channel(self):
        self.hitbtc_client.start_connection()
        self.hitbtc_client.subscribe_socket('BCN-BTC')
        self.hitbtc_client.close_connection()

    def test_listen_channel(self):
        self.hitbtc_client.start_connection()
        self.hitbtc_client.subscribe_socket('BCN-BTC')
        self.hitbtc_client.listen()
        time.sleep(5)
        self.hitbtc_client.close_connection()


class TestClientManagerHitBTC:

    hitbtc_manager = ClientsManager(config={'HitBTC': ['BCN-BTC']})

    def test_update_message(self):
        message = dict(event='Update',exchange='HitBTC',data=dict(symbol='BCN-BTC',time='time'))
        self.hitbtc_manager.listening = True
        self.hitbtc_manager.update(message)