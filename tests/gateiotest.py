import sys
sys.path.insert(0,"..")

from pycryptosockets.datafeeders.gateio_ws import GateIOWebsocketClient
import requests

def gateio_coins():
    r = requests.get("https://data.gate.io/api2/1/pairs")
    data = r.json()
    pairs = ["{}-{}".format(coin.split("_")[0], coin.split("_")[1]) for coin in data]
    return pairs, data

if __name__ == '__main__':
    pairs, coins = gateio_coins()
    print(len(coins))
    client = GateIOWebsocketClient(reconnect_on_error=False)
    client.start_connection()
    client.subscribe_socket(coins)
    client.coins = dict(zip(coins, pairs))
    client.listen()
    