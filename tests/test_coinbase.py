import pytest
import time


from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.coinbase_ws import COINBASEPROWebsocketClient


class TestCOINBASEPROWebsocketClient:
    coinbase_client = COINBASEPROWebsocketClient(reconnect_on_error=False)
    message_coinbase = '{"type":"ticker","sequence":10821431849,"product_id":"BTC-USD","price":"10175.06000000","open_24h":"10108.18000000","volume_24h":"8451.81867829","low_24h":"9851.97000000","high_24h":"10297.12000000","volume_30d":"342015.61161460","best_bid":"10175.06","best_ask":"10177.46","side":"sell","time":"2019-09-12T00:13:20.154000Z","trade_id":73884022,"last_size":"0.02963210"}'

    def test_connection_client(self):
        self.coinbase_client.start_connection()
        time.sleep(2)
        self.coinbase_client.close_connection()

    def test_subscribe_channel(self):
        self.coinbase_client.start_connection()
        self.coinbase_client.subscribe_socket('BTC-USDT')
        time.sleep(1)
        self.coinbase_client.close_connection()

    def test_on_message(self):
        self.coinbase_client.on_message(self.message_coinbase)


class TestClientManagerCoinBase:
    coinbase_manager = ClientsManager(config=dict(CoinBase=['BTC-USD']))

    def test_update_message(self):
        message = dict(
            event='Update',
            exchange='Bibox',
            data=dict(symbol='BTC-USDT', time='time',)
        )
        self.coinbase_manager.listening = True
        self.coinbase_manager.update(message)
