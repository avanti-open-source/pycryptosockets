import sys
sys.path.insert(0, "..")
from pycryptosockets.datafeeders import hitbtc_ws_test
from pycryptosockets.datafeeders._coins import hitbtc_coins

p, s = hitbtc_coins()

client = hitbtc_ws_test.HitBTC_testWebsocketClient(False)
client.start_connection()
client.coins = dict(zip(s, p))
client.subscribe_socket(s)
client.listen()

