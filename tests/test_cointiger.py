import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.cointiger_ws import CoinTigerWebsocketClient


class TestCoinTigerWebsocketClient:

    cointiger_client = CoinTigerWebsocketClient(reconnect_on_error=False)
    bytes_message = b'\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\x00\x15\x8a;\x0e\x80 \x10\x05\xef\xb2\xb5\x8d \xc4x\x19\xc2g\x13\r\x8afy\xd8\x18\xef.v3\x93y(\xae\xbe\x14\xdei\xa1\xc3Kf\xb8\x80\xd8j\x82\x83\xf8\xc4\x0e[\xcc,4\x10\xdf\\\xe0\x84\xaf~\xd6\x168\xf5V\xe1\xd1j\x0fg\xee\x86\x9fFc\xad\x9e\xecl\x94\x1e\x15\xbd\x1f\x89\xffS\x1a`\x00\x00\x00'
    cointiger_client.coins = dict(BTCUSDT='BTC-USDT')

    @pytest.mark.skip(reason='ws currently is down')
    def test_connection_client(self):
        self.cointiger_client.start_connection()
        self.cointiger_client.close_connection()

    @pytest.mark.skip(reason='ws currently is down')
    def test_subscribe_channel(self):
        self.cointiger_client.start_connection()
        self.cointiger_client.subscribe_socket('BTC-USDT')
        time.sleep(2)
        self.cointiger_client.close_connection()

    @pytest.mark.skip(reason='ws currently is down')
    def test_listen_channel(self):
        self.cointiger_client.start_connection()
        self.cointiger_client.subscribe_socket('BTC-USDT')
        self.cointiger_client.listen()
        time.sleep(2)
        self.cointiger_client.close_connection()

    @pytest.mark.skip(reason='ws currently is down')
    def test_on_message(self):
        self.cointiger_client.start_connection()
        self.cointiger_client.subscribe_socket('BTC-USDT')
        self.cointiger_client.on_message(self.bytes_message)
        self.cointiger_client.close_connection()


class TestClientManagerCoinTiger:

    cointiger_manager = ClientsManager(config={'CoinTiger': ['BTC-USDT']})

    def test_update_message(self):
        message = dict(event='Update',exchange='CoinTiger',data=dict(symbol='BTC-USDT',time='time'))
        self.cointiger_manager.listening = True
        self.cointiger_manager.update(message)