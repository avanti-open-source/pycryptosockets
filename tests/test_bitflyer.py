import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.bitflyer_ws import BitFlyerWebsocketClient


class TestBitFlyerWebsocketClient:

    bitflyer_client = BitFlyerWebsocketClient(reconnect_on_error=False)

    def test_connection_client(self):
        self.bitflyer_client.start_connection()
        time.sleep(5)
        self.bitflyer_client.close_connection()

    def test_subscribe_channel(self):
        self.bitflyer_client.start_connection()
        self.bitflyer_client.subscribe_socket('BTC-JPY')
        time.sleep(3)
        self.bitflyer_client.close_connection()

    def test_listen_channel(self):
        self.bitflyer_client.start_connection()
        self.bitflyer_client.subscribe_socket('BTC-JPY')
        self.bitflyer_client.listen()
        time.sleep(3)
        self.bitflyer_client.close_connection()


class TestClientManagerOkex:

    bitflyer_manager = ClientsManager(config={'Bitflyer': ['BTC-JPY']})

    def test_update_message(self):
        message = dict(event='Update',exchange='Bitflyer',data=dict(symbol='BTC-JPY',time='time'))
        self.bitflyer_manager.listening = True
        self.bitflyer_manager.update(message)
