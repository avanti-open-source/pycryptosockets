import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.bitmax_ws import BitmaxWebsocketClient


class TestBitmaxWebsocketClient:

    bitmax_client = BitmaxWebsocketClient(reconnect_on_error=False)

    @pytest.mark.skip(reason='ws unanavailable')
    def test_connection_client(self):
        self.bitmax_client.start_connection()
        self.bitmax_client.close_connection()

    @pytest.mark.skip(reason='ws unanavailable')
    def test_subscribe_channel(self):
        self.bitmax_client.start_connection()
        self.bitmax_client.subscribe_socket('ETH-BTC')
        self.bitmax_client.close_connection()

    @pytest.mark.skip(reason='ws unanavailable')
    def test_listen_channel(self):
        self.bitmax_client.start_connection()
        self.bitmax_client.subscribe_socket('ETH-BTC')
        self.bitmax_client.listen()
        time.sleep(5)
        self.bitmax_client.close_connection()


class TestClientManagerBitmex:

    bitmex_manager = ClientsManager(config={'Bitmax': ['ETH-BTC']})

    def test_update_message(self):
        message = dict(
            event='Update',
            exchange='Bitmax',
            data=dict(
                symbol='ETH-BTC',
                time='time',
            ),
        )
        self.bitmex_manager.listening = True
        self.bitmex_manager.update(message)