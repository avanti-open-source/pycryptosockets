import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.oex_ws import OEXWebsocketClient


class TestOEXWebsocketClient:

    oex_client = OEXWebsocketClient(reconnect_on_error=False)
    bytes_message = b'\x1f\x8b\x08\x00\x00\x00\x00\x00\x00\xffl\xceAk\x840\x10\x05\xe0\xbfR\xde9\xbbD\xc3\xd88\xf7\xde\xf6\xd6c)\x12\xcc@E7.\xc9\xb8\x14\xc4\xff^"=\xeeq\x1e\xf3\xf1\xde\x0eyJ\xd2!\xcb\x03\x0c\x18\x8c?!%Y\xc0\xb8\x87<\x8b\x0e\xab\xfcn%\xea\xa09D\x19t\x1ag\xc90\x88A\x038m\xcbbPC\xf0\xfe\x9f}\xed\x08\xf7uK\n\x86\xbd:\xea\xbdm*(`\xb4\xb6\xe9/\xd6_\x1az\xb3\x9e]\xc7-\xc1`\x8a\xe0\xf7\xceQ\xeb\r\x1ey\x1a\xe5\x94\xa7*S\xac\xd7\xe7\xc7\xed\x06\x03-\xe0\x86:\xf2m\xef<Yk\r\x9ek\xdd\xea\xe8z\xd6\x1c\xdf/\x9e\x8e\x97\xb0h\xd0\xadnZg\x1c\x7f\x00\x00\x00\xff\xff\x01\x00\x00\xff\xff\x86\x12\xc0\xa1\x07\x01\x00\x00'


    @pytest.mark.skip(reason='ws currently is down')
    def test_connection_client(self):
        self.oex_client.start_connection()
        self.oex_client.close_connection()


    @pytest.mark.skip(reason='ws currently is down')
    def test_subscribe_channel(self):
        self.oex_client.start_connection()
        self.oex_client.subscribe_socket('BTC-USDT')


    @pytest.mark.skip(reason='ws currently is down')
    def test_on_message(self):
        self.oex_client.start_connection()
        self.oex_client.on_message(self.bytes_message)
        self.oex_client.close_connection()


    @pytest.mark.skip(reason='ws currently is down')
    def test_listen_channel(self):
        self.oex_client.start_connection()
        self.oex_client.subscribe_socket('BTC-USDT')
        self.oex_client.listen()
        time.sleep(5)
        self.oex_client.close_connection()


class TestClientManagerOEX:

    oex_manager = ClientsManager(config={'OEX': ['BTC-USDT']})


    @pytest.mark.skip(reason='ws currently is down')
    def test_update_message(self):
        message = dict(event='Update',exchange='OEX',data=dict(symbol='BTC-USDT',time='time'))
        self.oex_manager.listening = True
        self.oex_manager.update(message)