import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.gateio_ws import GateIOWebsocketClient


class TestGateIOWebsocketClient:

    gateio_client = GateIOWebsocketClient(reconnect_on_error=False)

    def test_connection_client(self):
        self.gateio_client.start_connection()
        self.gateio_client.close_connection()

    def test_subscribe_channel(self):
        self.gateio_client.start_connection()
        self.gateio_client.subscribe_socket('DOGE-USDT')
        self.gateio_client.close_connection()

    def test_listen_channel(self):
        self.gateio_client.start_connection()
        self.gateio_client.subscribe_socket('DOGE-USDT')
        self.gateio_client.listen()
        time.sleep(5)
        self.gateio_client.close_connection()


class TestClientManagerGateIO:

    gateio_manager = ClientsManager(config={'GateIO': ['DOGE-USDT']})

    def test_update_message(self):
        message = dict(event='Update',exchange='GateIO',data=dict(symbol='DOGE-USDT',time='time'))
        self.gateio_manager.listening = True
        self.gateio_manager.update(message)