import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.okex_ws import OkexWebsocketClient


class TestOkexWebsocketClient:

    okex_client = OkexWebsocketClient(reconnect_on_error=False)

    def test_connection_client(self):
        self.okex_client.start_connection()
        time.sleep(5)
        self.okex_client.close_connection()

    def test_subscribe_channel(self):
        self.okex_client.start_connection()
        self.okex_client.subscribe_socket('IOTA-OKB')
        time.sleep(5)
        self.okex_client.close_connection()

    def test_listen_channel(self):
        self.okex_client.start_connection()
        self.okex_client.subscribe_socket('IOTA-OKB')
        self.okex_client.listen()
        time.sleep(5)
        self.okex_client.close_connection()


class TestClientManagerOkex:

    okex_manager = ClientsManager(config={'Okex': ['IOTA-OKB']})

    def test_update_message(self):
        message = dict(event='Update',exchange='Okex',data=dict(symbol='IOTA-OKB',time='time'))
        self.okex_manager.listening = True
        self.okex_manager.update(message)