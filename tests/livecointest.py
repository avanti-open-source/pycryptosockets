import sys
sys.path.insert(0,"..")

from pycryptosockets.datafeeders.livecoin_ws import LivecoinWebsocketClient
import requests

def livecoin_coins():
    r = requests.get("https://api.livecoin.net/exchange/ticker")
    data = r.json()
    pairs = [x['symbol'].replace("/","-") for x in data]
    symbols = [x['symbol'] for x in data]
    return pairs, symbols

if __name__ == '__main__':
    pairs, coins = livecoin_coins()
    print(len(coins))
    client = LivecoinWebsocketClient(reconnect_on_error=False)
    client.start_connection()
    client.subscribe_socket(coins)
    client.coins = dict(zip(coins, pairs))
    client.listen()
    