# -*- coding: utf-8 -*-
"""
Created on Mon Jul  9 18:25:43 2018

@author: arnaldo
"""
import sys
sys.path.insert(0,"..")
from pycryptosockets.datafeeders import zaif_ws as zw


pairs = ['btc_jpy', 'xem_jpy', 'mona_jpy', 'bch_jpy',
         'eth_jpy', 'xem_btc', 'mona_btc', 'bch_btc', 'eth_btc']

x = [i.split('_')[0].upper() + '-' + i.split('_')[1].upper() for i in pairs]

client = zw.ZaifWebsocketClient(False)
client.coins = dict(zip(pairs, x))
client.subscribe_socket('btc_jpy')
client.listen()
