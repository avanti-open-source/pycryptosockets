import pytest
import time
from threading import Thread

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.binance_ws import BinanceWebsocketClient


auth = {
    'Binance': {
        'api_key': 'NV3kgjkqO2Ofuj2kDLb1sN7DLZhkcO3qSV44p5ZXipr57YvUQ7MmRSUndESqmTtd',
        'api_secret': 'Jl2JS1LAvkFo2fMkpURdCUdAsdJcIUNlZWSFN9JzNlAxJx4InaanKIlDbHvJNh3W'
    }
}


class TestBinanceWebsocketClient:

    binance_client = BinanceWebsocketClient(reconnect_on_error=False, kwargs=auth)
    incoming_message = {'stream': 'xrpbtc@trade', 'data': {'e': 'trade', 'E': 1566261369000, 's': 'XRPBTC', 't': 56758073, 'p': '0.00002563', 'q': '678.00000000', 'b': 221731175, 'a': 221731165, 'T': 1566261368993, 'm': False, 'M': True}}


    def test_connection_client(self):
        self.binance_client.start_connection()
        time.sleep(2)
        self.binance_client.close_connection()

    def test_subscribe_channel(self):
        self.binance_client.start_connection()
        self.binance_client.subscribe_socket('XRP-BTC')
        time.sleep(2)
        self.binance_client.close_connection()

    def test_on_message(self):
        self.binance_client.start_connection()
        self.binance_client.coins = dict(XRPBTC='XRP-BTC')
        self.binance_client.on_message(self.incoming_message)
        self.binance_client.close_connection()


class TestClientManagerBinance:

    binance_manager = ClientsManager(config={'Binance': ['XRP-BTC']},auth=auth)

    def test_update_message(self):
        message = dict(event='Update',exchange='Binance',data=dict(symbol='XRP-BTC',time='time'))
        self.listening = True
        self.binance_manager.listening = True
        self.binance_manager.update(message)