import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.bitbank_ws import BitbankWebsocketClient


auth = dict(
    Bitbank=dict(subscribe_key='sub-c-e12e9174-dd60-11e6-806b-02ee2ddab7fe')
)


class TestBitbankWebsocketClient:

    bitbank_client = BitbankWebsocketClient(reconnect_on_error=False, kwargs=auth)
    message = {'pid': 800828134, 'data': {'transactions': [{'transaction_id': 33947403, 'side': 'sell', 'price': '1164000', 'amount': '0.0096', 'executed_at': 1566264208960}]}}

    def test_connection_channel(self):
        self.bitbank_client.subscribe_socket('BTC-JPY')
        self.bitbank_client.close_connection()

    def test_subscribe_channel(self):
        self.bitbank_client.start_connection()
        self.bitbank_client.subscribe_socket('BTC-JPY')
        self.bitbank_client.close_connection()

    def test_on_message(self):
        self.bitbank_client.start_connection()
        self.bitbank_client.coins = {'btc_jpy': 'BTC-JPY'}
        self.bitbank_client.on_message(self.message, sym='transactions_btc_jpy')

class TestClientManagerBitbank:

    bitbank_manager = ClientsManager(config={'Bitbank': ['BTC-JPY']})

    def test_update_message(self):
        message = dict(event='Update',exchange='Bitbank',data=dict(symbol='BTC-JPY',time='time'))
        self.bitbank_manager.listening = True
        self.bitbank_manager.update(message)