import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.kraken_ws import KrakenWebsocketClient


class TestKrakenWebsocketClient:

    kraken_client = KrakenWebsocketClient(reconnect_on_error=False)
    message = '[496,[["0.024560","8.10000000","1588206552.229678","s","m",""]],"trade","ETH/XBT"]'
    subscribe_message = '{"channelID":31,"channelName":"trade","event":"subscriptionStatus","pair":"USD/JPY","status":"subscribed","subscription":{"name":"trade"}}'

    def test_connection_client(self):
        self.kraken_client.start_connection()
        self.kraken_client.close_connection()

    def test_subscribe_channel(self):
        self.kraken_client.start_connection()
        self.kraken_client.subscribe_socket('ETH-XBT')
        self.kraken_client.close_connection()

    def test_listen_channel(self):
        self.kraken_client.start_connection()
        self.kraken_client.subscribe_socket('ETH-XBT')
        self.kraken_client.listen()
        time.sleep(5)
        self.kraken_client.close_connection()

    def test_on_message(self):
        self.kraken_client.coins = {'ETH/XBT': 'ETH-XBT'}
        self.kraken_client.on_message(self.message)
        self.kraken_client.on_message(self.subscribe_message)


class TestClientManagerKraken:

    kraken_manager = ClientsManager(config={'Kraken': ['ETH-XBT']})

    def test_update_message(self):
        message = dict(event='Update',exchange='Kraken',data=dict(symbol='ETH-XBT',time='time'))
        self.kraken_manager.listening = True
        self.kraken_manager.update(message)