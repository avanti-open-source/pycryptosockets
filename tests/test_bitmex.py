import pytest
import time

from pycryptosockets import ClientsManager
from pycryptosockets.datafeeders.bitmex_ws import BitmexWebsocketClient


class TestBitmexWebsocketClient:

    bitmex_client = BitmexWebsocketClient(reconnect_on_error=False)

    def test_connection_client(self):
        self.bitmex_client.start_connection()
        self.bitmex_client.close_connection()

    def test_subscribe_channel(self):
        self.bitmex_client.start_connection()
        self.bitmex_client.subscribe_socket('XBTUSD')
        self.bitmex_client.close_connection()

    def test_listen_channel(self):
        self.bitmex_client.start_connection()
        self.bitmex_client.subscribe_socket('XBTUSD')
        self.bitmex_client.listen()
        time.sleep(5)
        self.bitmex_client.close_connection()


class TestClientManagerBitmex:

    bitmex_manager = ClientsManager(config={'Bitmex': ['XBTUSD']})

    def test_update_message(self):
        message = dict(event='Update',exchange='Bitmex',data=dict(symbol='XBTUSD',time='time'))
        self.bitmex_manager.listening = True
        self.bitmex_manager.update(message)