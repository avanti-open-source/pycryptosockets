from loguru import logger

from pycryptosockets import datafeeders as pdf
from pycryptosockets.datafeeders import _coins


class CoinsManager:
    """ Class to validate coins for exchanges and provide the clients."""
    def __init__(self):
        self.pcs_to_exch = {}

    def get_client(self, exchange, auth, on_error):
        """
        Function to get the client for a especific exchange.

        :param exchange: str with exchange name
        :return: WebsocketClient
        """
        client_class = getattr(pdf, "{}WebsocketClient".format(exchange))
        client = client_class(on_error, kwargs=auth)
        get_coins_name = exchange.lower() + "_coins"
        get_coins_func = getattr(_coins, get_coins_name)
        upper_coins, coins = get_coins_func()
        self.pcs_to_exch[exchange] = dict(zip(upper_coins, coins))
        return client

    def get_validate_pairs(self, exchange, pairs):
        """
        From a list of pairs returns the valid ones for the especific exchange.

        :param exchange: str with exchange name
        :param pairs: list with pairs to validate
        :return: list with valid coins in correct format
        """
        valid_list = {}
        if pairs in ['all', 'ALL']:
            pairs_all = self.pcs_to_exch[exchange]
            for coin in pairs_all:
                if coin in self.pcs_to_exch[exchange]:
                    valid_list[self.pcs_to_exch[exchange][coin]] = coin
                elif self.usd_equal_usdt and coin+"T" in self.pcs_to_exch[exchange]:
                    # This part searchs for pairs with USDT quotes
                    valid_list[self.pcs_to_exch[exchange][coin+"T"]] = coin
        else:
            for coin in pairs:
                if coin in self.pcs_to_exch[exchange]:
                    valid_list[self.pcs_to_exch[exchange][coin]] = coin
                elif self.usd_equal_usdt and coin+"T" in self.pcs_to_exch[exchange]:
                    # This part searchs for pairs with USDT quotes
                    valid_list[self.pcs_to_exch[exchange][coin+"T"]] = coin

                else:
                    logger.info("{} not in {}.".format(coin, exchange))

        return valid_list
