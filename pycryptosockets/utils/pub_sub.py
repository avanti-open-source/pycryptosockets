from loguru import logger


class Subscriber:
    def __init__(self, name, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.name = name

    def update(self, message):
        # start new Thread in here to handle any task
        logger.info(f"\n\n {self.name} got message {message}")
        pass


class Publisher:
    def __init__(self, channels, *args, **kwargs):
        # maps channel names to subscribers
        # str -> dict
        super().__init__(*args, **kwargs)
        self.channels = {channel: dict()
                         for channel in channels}

    def get_subscribers(self, channel):
        return self.channels[channel]

    def get_channels(self):
        return self.channels

    def register(self, channel, subscriber):
        self.get_subscribers(channel)[subscriber] = subscriber.update

    def unregister(self, channel, subscriber):
        del self.get_subscribers(channel)[subscriber]

    def dispatch(self, channel, message):
        """
        Sends the message to all the objects subscribed to the
        channel.
        """
        for _, callback in self.get_subscribers(channel).items():
            self.run(callback, message)

    def run(self, callback, message):
        callback(message)
