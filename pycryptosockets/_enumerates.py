pycrypto_exchanges = [
    'Bitfinex', 'Binance', 'GDAX',
    'Okex', 'Poloniex', 'Bitstamp',
    'HitBTC', 'Bittrex', 'Bitbank',
    'Huobipro', 'GateIO', 'Livecoin',
    'BitFlyer', 'Zaif', 'Kucoin',
    'Bitmex', 'OEX', 'Bitmax',
    'CoinTiger', 'Bibox', 'FatBTC',
    'Bitz', 'IDAX', 'COINBASEPRO',
    'P2PB2B', 'CoinsBit', 'Kraken',
]

needs_auth = ['Binance', 'Bitbank']
