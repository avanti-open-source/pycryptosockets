"""
pycryptosockets


The unified cryptocurrencies websockets module
Powered by Avanti Financial Services
"""
from ._base import BaseWebsocketClient
from ._core import ClientsManager
from .utils.coinsmanager import CoinsManager

__version__ = "0.2.9.4"
