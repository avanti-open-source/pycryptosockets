import sys

import gevent.monkey

from loguru import logger
from threading import Thread
from .utils.pub_sub import Subscriber
from ._enumerates import pycrypto_exchanges, needs_auth
from .utils.coinsmanager import CoinsManager


class ClientsManager(Subscriber, CoinsManager):
    """Class ment to unify all the clients of pycryptosockets in one class."""

    def __init__(self, name='ClientsManager', exchanges=None,
                 pairs=None, config=None, auth=None, usd_equal_usdt=False,
                 reconnect_on_error=True, debug=False,
                 *args, **kwargs):
        """
        Builder Method.

        :param name: Name of the subscriber
        :param exchange: list of the exchanges
        :param pairs: list of pairs to subscribe
        :param config: custom dictionary for detailed subscriptions
        """
        super().__init__(name, *args, **kwargs)
        self.exchanges = exchanges
        self.debug = debug
        self.pairs = pairs
        self.config = config
        self.auth = auth
        self.clients = []
        self.usd_equal_usdt = usd_equal_usdt
        self.on_error = reconnect_on_error
        self.__ok = True
        self.__check_entries()

    def __check_entries(self):
        """Verify that all the params are correct."""
        if self.config:
            self.exchanges = self.config.keys()
            self.pairs = True

        if self.exchanges and self.pairs:
            for exchange in self.exchanges:
                if exchange in needs_auth and self.auth:
                    if exchange not in self.auth:
                        logger.warning("{} need auth! Please provide the keys.".format(exchange))
                        self.__ok = False
                        break
                elif exchange in needs_auth and not self.auth:
                    logger.warning("{} need auth! Please provide the keys.".format(exchange))
                    self.__ok = False
                    break
        else:
            self.__ok = False
            logger.warning("You need to provide either Exchanges and Pairs or a custom configuration!")

    def update(self, message):
        """Method to receive messages."""
        if self.listening and message is not None:
            if self.debug:
                try:
                    if message['event'] == 'Update':
                        exchange = message['exchange']
                        symbol = message['data']['symbol']
                        datetime = message['data']['time']
                        logger.debug(f"{exchange}, {symbol}, {datetime}")
                except KeyError as e:
                    logger.error(f"Error on key: {e}, {message}")

    def _get_datafeeders(self):
        """
        Check if the exchanges were correctly invoqued.

        and searchs for the client.
        """
        if self.config:
            self.exchanges = list(self.config.keys())
        for exchange in self.exchanges:
            if exchange in pycrypto_exchanges:
                self._initialize_exchange_socket(exchange)
                logger.info(f"Registered to {exchange}")
            else:
                logger.info(f"Unavailable exchange: {exchange}")
                self.exchanges.remove(exchange)

    def _initialize_exchange_socket(self, exchange):
        """
        Get the exchange client and appends it to the clients list.

        clients list is a list of tuples where the first position is
        the name and the second is the client
        :param exchange: exchange name
        """
        wsclient = self.get_client(exchange, self.auth, self.on_error)
        # The default channel of the clients when created
        # should be data_feeder
        wsclient.pub.register('data_feeder', self)
        self.clients.append((exchange, wsclient))

    def _subscribe_to_data(self):
        """Subscribe all the exchanges to its coins."""
        if self.config:
            self._subscribe_config()
        else:
            self.config = {}
            for client in self.clients:
                exchange, wsclient = client
                wsclient.start_connection()
                logger.info(f"Connected to {exchange}!")
                # available pairs for exchange
                valid_pairs = self.get_validate_pairs(exchange, self.pairs)
                self.config[exchange] = valid_pairs
                wsclient.coins = valid_pairs
                wsclient.subscribe_socket(list(valid_pairs.keys()))

    def _subscribe_config(self):
        """
        Subscribe the custom config.

        Custom config is a dictionary with the following structure:
        config = {
                    'Exchange1' = [list, of, coins, for, this, exchange],
                    'Exchange2' = [list, of, coins, for, this, other, exchange],
                    ...
                }
        """
        for client in self.clients:
            exchange, wsclient = client
            try:
                wsclient.start_connection()
                logger.info(f"Connected to {exchange}!")
                # available pairs for exchange
                valid_pairs = self.get_validate_pairs(exchange, self.config[exchange])
                self.config[exchange] = valid_pairs
                wsclient.coins = valid_pairs
                wsclient.subscribe_socket(list(valid_pairs.keys()))
            except Exception as e:
                logger.error(e)

    def _listen(self):
        logger.info("Starting to listen...")
        self.threads = []
        for client in self.clients:
            exch, wsclient = client
            if len(self.exchanges) > 1:
                t = Thread(target=wsclient.listen)
                logger.info("Listening to", exch)
                t.start()
                self.threads.append(t)
                logger.info("Done! Enjoy the data!")
            else:
                logger.info("Starting only one")
                self.listening = True
                wsclient.listen()

    def start(self):
        """Data reception begins."""
        if self.__ok:
            self.listening = False
            logger.info("Hold on! We're setting everything up.")
            self._get_datafeeders()
            self._subscribe_to_data()
            self._listen()
            self.listening = True
        else:
            logger.info("Can't start with bad entries.")
