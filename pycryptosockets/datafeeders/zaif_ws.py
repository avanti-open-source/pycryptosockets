import json
import datetime as dt
import pytz

from .._base import BaseWebsocketClient
from dateutil import parser
from websocket import create_connection


class ZaifWebsocketClient(BaseWebsocketClient):
    """
    Class to receive and deliver Zaif trades data.
    """
    def __init__(self, reconnect_on_error, *args, **kwargs):
        self.url = "wss://ws.zaif.jp/stream?currency_pair="
        self.new_trades = []
        super().__init__('Zaif', self.url, channels=['data_feeder'],
                         reconnect_on_error=reconnect_on_error)

    def start_connection(self):
        pass

    def subscribe_socket(self, pair):
        """
        Custom subscription for zaif websocket, for the currencies
        available in the exchange.
        :param pair: list of currencies pair, ['BTC-JPY']
        :return:
        """
        pair = pair[0]
        self.localzone = pytz.timezone('Japan')
        self._ws = create_connection(self.url + pair)

    def on_message(self, msg):
        """
        Custom on_message for zaif_ws
        """
        if msg != '':
            mess = json.loads(msg)
            for trade in mess['trades']:
                tid = trade['tid']
                if tid not in self.new_trades:
                    self.new_trades.append(tid)
                    sym = self.coins[trade['currency_pair']]
                    t = parser.parse(mess['timestamp'])
                    t = t - dt.timedelta(hours=9)

                    data = {'symbol': sym,
                            'price': trade['price'],
                            'volume': trade['amount'],
                            'side': "BUY" if trade['trade_type'] == "bid" else "SELL",
                            'time': t.isoformat() + "+00:00"}

                    mdata = {"event": "Update", "data": data, "exchange": self.name}
                    self.pub.dispatch(self.channels[0], mdata)
            self.new_trades = self.new_trades[-100:]
