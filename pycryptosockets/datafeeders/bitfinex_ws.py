from .._base import BaseWebsocketClient
import json
import datetime as dt


class BitfinexWebsocketClient(BaseWebsocketClient):
    """Class to recive and deliver Bitfinex trades data."""

    def __init__(self, reconnect_on_error, *args, **kwargs):
        """Builder method."""
        self.url = 'wss://api.bitfinex.com/ws/2'
        self.coins_dict = {}
        self.heartbeats = 0
        super().__init__('Bitfinex', self.url, channels=['data_feeder'],
                         reconnect_on_error=reconnect_on_error)

    def subscribe_socket(self, pairs):
        """Custom subscription."""
        for coin in pairs:
            dict_ = {"event": "subscribe",
                     "channel": "trades",
                     "pair": "t"+coin.upper()}
            self.send_message(dict_)

    def on_message(self, msg):
        mess = json.loads(msg)
        if isinstance(mess, dict):
            if mess['event'] == 'subscribed':
                self.coins_dict[mess['chanId']] = mess['pair']
        else:
            if len(mess)>2 and mess[1] == 'te':
                sym = self.coins[self.coins_dict[mess[0]].lower()]
                t = dt.datetime.fromtimestamp(mess[2][1]/1000, self.localzone)
                t = t.astimezone(self.utczone)
                mess_d = {
                            'symbol': sym,
                            'price' : mess[2][3],
                            'volume': abs(mess[2][2]),
                            'side': 'BUY' if mess[2][2] > 0 else 'SELL',
                            'time': t.isoformat()
                        }
                data = {"event": "Update", "data": mess_d, "exchange": self.name}
                self.pub.dispatch('data_feeder', data)
            else:
                self.heartbeats += 1
                if self.heartbeats == len(self.coins_dict):
                    data = {"event": "Heartbeat", "exchange":self.name}
                    self.pub.dispatch('data_feeder', data)
                    self.heartbeats = 0
