import json
import requests

from datetime import datetime as dt
from .._base import BaseWebsocketClient


class KucoinWebsocketClient(BaseWebsocketClient):
    """Class to receive and deliver Kucoin trades data."""

    def __init__(self, reconnect_on_error, *args, **kwargs):
        """Built method."""
        self.request_server = requests.post('https://api.kucoin.com/api/v1/bullet-public').text
        self.response = json.loads(self.request_server)
        self.token = self.response['data']['token']
        # self.url = f"wss://push1-v2.kucoin.com/endpoint?token={self.token}"
        self.url = f"{self.response['data']['instanceServers'][0]['endpoint']}?token={self.token}"
        super().__init__(
            'Kucoin',
            self.url,
            channels=['data_feeder'],
            reconnect_on_error=reconnect_on_error,
        )

    def subscribe_socket(self, pairs):
        if len(pairs) >= 300:
            dict_ = {
                'id': 12345432,
                'type': "subscribe",
                'topic': '/market/ticker:all',
                'response': True,
            }
        else:
            for coin in pairs:
                dict_ = {"id": "{}".format(coin),
                        "type": "subscribe",
                        "topic": f"/market/ticker:{coin}",
                        "response": True}
        self.send_message(dict_)

    def on_message(self, msg):
        """
        May be overriden.

        Handle the message and dispatch it.
        :param msg: String with message
        """
        mess = json.loads(msg)
        if mess.get('data'):
            try:
                time = dt.fromtimestamp(int(mess['data']['sequence'])/1000)
                time = time.astimezone(self.utczone)
                symbol = mess['topic'].split(':')[-1]
                mess_d = {
                    'symbol': symbol if symbol != 'all' else mess['subject'],
                    'price': float(mess['data']['price']),
                    'volume': float(mess['data']['size']),
                    'side': mess['subject'] if symbol != 'all' else 'N/A',
                    'time': time.isoformat()
                }
                data = {"event": "Update",
                        "data": mess_d,
                        "exchange": self.name}
                self.pub.dispatch(self.channels[0], data)
            except Exception as e:
                print('Exception as occured {}'.format(e))
