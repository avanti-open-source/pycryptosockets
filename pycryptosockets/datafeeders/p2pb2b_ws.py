import json

from time import sleep
from datetime import datetime as dt

from .._base import BaseWebsocketClient


class P2PB2BWebsocketClient(BaseWebsocketClient):

    def __init__(self, reconnect_on_error, *args, **kwargs):
        self.url = 'wss://apiws.p2pb2b.io/'
        super().__init__(
            name='P2PB2B',
            url=self.url,
            channels=['data_feeder'],
            reconnect_on_error=reconnect_on_error,
            header={
                'content-type': 'application/json-rpc',
                'accept': 'application/json-rpc'
            }
        )

    def subscribe_socket(self, pairs):
        subscribe = {
            "jsonrpc": "2.0",
            "method": "deals.subscribe",
            "params": pairs,
            "id": 0
        }
        self.send_message(subscribe)

    def on_message(self, message):
        message_json = json.loads(message)
        if message_json.get('params'):
            pair, deals = message_json['params']
            pair = pair.replace('_', '-')
            for deal in deals:
                message_format = dict(
                    symbol=pair,
                    price=float(deal['price']),
                    volume=float(deal['amount']),
                    side=deal['type'],
                    time=dt.fromtimestamp(deal['time'], self.localzone).isoformat(),
                )
                data = {
                    "event": "Update",
                    "data": message_format,
                    "exchange": self.name
                }
                self.pub.dispatch('data_feeder', data)
