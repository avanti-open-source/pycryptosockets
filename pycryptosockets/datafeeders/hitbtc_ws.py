import json
import time

from loguru import logger
from websocket import create_connection
from threading import Thread
from .._base import BaseWebsocketClient


class HitBTCWebsocketClient(BaseWebsocketClient):
    """
    Class to recive and deliver HitBTC trades data
    """

    def __init__(self, reconnect_on_error, *args, **kwargs):
        self.url = 'wss://api.hitbtc.com/api/2/ws'
        super().__init__('HitBTC', self.url, channels=['data_feeder'],
                         reconnect_on_error=reconnect_on_error, ping_interval=15)

    def start_connection(self):
        self._ws = create_connection(self.url)
        t = Thread(target=self.ping_server)
        t.start()

    def subscribe_socket(self, pairs):
        """
        subscriptions for the currencies available in the exchange of HitBTC.
        :param pairs: list of coin pairs.
        :return:
        """
        if len(pairs) >= 50:
            logger.warning("##### HitBTC connection could be fail #####")

        for coin in pairs:
            request = {
                "method": "subscribeTrades",
                "params": {
                    "symbol": coin,
                }
            }

            self.send_message(request)

    def on_message(self, msg):
        """
        Custom on_message.
        :param msg: data of the currency to which you have subscribed.
        :return:
        """

        message = json.loads(msg)
        if message.get('method') and message['method'] == 'updateTrades':
            symbol_format = message['params']['symbol']
            symbol = self.coins[symbol_format]

            for trades in message['params']['data']:
                message_format = {
                    'symbol': symbol,
                    'price': trades['price'],
                    'volume': float(trades['quantity']),
                    'side': trades['side'].upper(),
                    'time': trades['timestamp'].replace('Z', '') + "+00:00",
                }

                data = {'event': 'Update', 'data': message_format, 'exchange': self.name}
                self.pub.dispatch('data_feeder', data)

    def ping_server(self):
        while self.go_on:
            time.sleep(self.ping)
            data = {"event": "Heartbeat", "exchange": self.name}
            self.pub.dispatch('data_feeder', data)
            self._ws.ping()
