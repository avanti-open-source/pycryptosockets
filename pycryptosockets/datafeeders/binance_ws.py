import datetime as dt
from binance.websockets import BinanceSocketManager
from binance.client import Client
from .._base import BaseWebsocketClient


class BinanceWebsocketClient(BaseWebsocketClient):
    """docstring for BinanceWebSocketClient."""

    def __init__(self, reconnect_on_error, *args, **kwargs):
        """
        Initializer.

        :param client: for :
        """
        self.url = ''
        key = kwargs['kwargs']['Binance']['api_key']
        secret = kwargs['kwargs']['Binance']['api_secret']
        client = Client(key, secret)
        self.channels = ['data_feeder']
        self.bm = BinanceSocketManager(client)
        super().__init__('Binance', self.url, channels=self.channels,
                         reconnect_on_error=reconnect_on_error)

    def start_connection(self):
        """Client that connects when listen."""
        pass

    def listen(self):
        """
        Main function to recieve messages.

        Should be on a thread
        """
        self.bm.start_multiplex_socket(self.multi,
                                       self.on_message)
        self.bm.start()

    def on_message(self, msg):
        """
        May be overriden.

        Handle the message and dispatch it.
        :param msg: String with json format
        """
        time = dt.datetime.fromtimestamp(msg['data']['T']/1000)
        t = time.astimezone(self.utczone)
        sym = self.coins[msg['data']['s']]
        msg = {
            'symbol': sym,
            'price': float(msg['data']['p']),
            'volume': float(msg['data']['q']),
            'side': 'Sell' if msg['data']['m'] else 'Buy',
            'time': t.isoformat()
        }
        data = {"event": "Update", "data": msg, "exchange": self.name}
        self.pub.dispatch(self.channels[0], data)

    def subscribe_socket(self, pairs):
        """Method that return a list of pairs."""
        self.multi = []
        for coin in pairs:
            trade = coin.lower() + "@trade"
            self.multi.append(trade)

    def close_connection(self):
        """
        Closes connection to the server
        """
        self.go_on = False
        self.bm.close()
