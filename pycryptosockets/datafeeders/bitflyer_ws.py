from .._base import BaseWebsocketClient
import json


class BitFlyerWebsocketClient(BaseWebsocketClient):
    """
    Class to receive and deliver BitFlyer trades data.
    """
    def __init__(self, reconnect_on_error, *args, **kwargs):
        self.url = 'wss://ws.lightstream.bitflyer.com/json-rpc'
        super().__init__('BitFlyer', self.url, channels=['data_feeder'],
                         reconnect_on_error=reconnect_on_error)

    def subscribe_socket(self, pairs):
        """
        Subscriptions for the currencies available in the exchange of BitFlyer.
        :param pairs:
        :return:
        """
        for pair in pairs:
            request = {"method": "subscribe", "params": {"channel": "lightning_executions_" + pair}}
            self.send_message(request)

    def on_message(self, msg):
        """
        Custom on_message for Bitflyer_ws
        :param msg:
        :return:
        """
        mess = json.loads(msg)
        params = mess['params']
        symbol = params['channel'].split('lightning_executions_')[1]
        for trades in params['message']:
            mess_format = {'symbol': self.coins[symbol],
                           'price': trades['price'],
                           'volume': trades['size'],
                           'side': trades['side'],
                           'time': trades['exec_date'].replace('Z', '') + "+00:00"}
            data = {"event": "Update", "data": mess_format, "exchange": self.name}
            self.pub.dispatch('data_feeder', data)
