import json
import gzip
import datetime as dt

from .._base import BaseWebsocketClient


class OEXWebsocketClient(BaseWebsocketClient):
    """Class to receive and deliver OEX trades data."""

    def __init__(self, reconnect_on_error, *args, **kwargs):
        """Built method."""
        self.url = 'wss://ws.oex.com/kline-api/ws'
        super().__init__(
            'OEX',
            self.url,
            channels=['data_feeder'],
            reconnect_on_error=reconnect_on_error,
        )

    def subscribe_socket(self, pairs):
        """Customize subscription."""
        for coin in pairs:
            dict_ = dict(
                event="sub",
                params=dict(
                    channel="market_{}_trade_ticker".format(coin),
                    cb_id="custom",
                ),
            )
            self.send_message(dict_)

    def on_message(self, msg):
        """
        May be overriden.

        Handle the message and dispatch it.
        :param msg: String with json format
        """

        message = json.loads(gzip.decompress(msg).decode('utf-8'))

        if message.get('channel'):
            pair = message['channel'].split('_')[1]
            if pair[-4:] == 'usdt':
                currency = pair[:-4] + '-' + pair[-4:]
            else:
                currency = pair[:-3] + '-' + pair[-3:]

            for trade in message['tick']['data']:
                time = dt.datetime.fromtimestamp(trade['ts']/1000, self.localzone)
                message_format = dict(
                    symbol=currency.upper(),
                    price=float(trade['price']),
                    volume=trade['vol'],
                    side=trade['side'],
                    time=time.isoformat(),
                )

                data = dict(event="Update", data=message_format, exchange=self.name)
                self.pub.dispatch('data_feeder', data)
        else:
            pong = '{"pong"}'
            self.send_message(pong)
            data = {"event": "Heartbeat", "exchange": self.name}
            self.pub.dispatch('data_feeder', data)
