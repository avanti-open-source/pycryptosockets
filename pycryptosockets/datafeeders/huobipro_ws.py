from .._base import BaseWebsocketClient
import json
import datetime as dt
import gzip

class HuobiproWebsocketClient(BaseWebsocketClient):
    """
    Class to receive and deliver Huobipro trades data
    """

    def __init__(self, reconnect_on_error, *args, **kwargs):
        self.url = 'wss://api.huobi.pro/ws'
        super().__init__('Huobipro', self.url, channels=['data_feeder'],
                         reconnect_on_error=reconnect_on_error)

    def subscribe_socket(self, pairs):
        """
        Subscriptions for the currencies available in the exchange of Huobipro.
        :param pairs: list of coin pairs.
        :return:
        """

        for pair in pairs:
            request = {'sub': 'market.' + pair + '.trade.detail',
                       'id': 'id10'}
            self.send_message(request)

    def on_message(self, msg):
        """
        Custom on_message.
        :param msg: data of the currency to which you have subscribed.
        :return:
        """
        mess = gzip.decompress(msg).decode('utf-8')
        if mess[:7] == '{"ping"':
            ts = mess[8:21]
            pong = '{"pong":' + ts + '}'
            self.send_message(pong)
            data = {"event": "Heartbeat", "exchange": self.name}
            self.pub.dispatch('data_feeder', data)

        else:
            mess = json.loads(mess)
            if 'id' not in mess.keys():
                for trades in mess['tick']['data']:
                    t = dt.datetime.fromtimestamp(trades['ts'] / 1e3, self.localzone)
                    t = t.astimezone(self.utczone)
                    mess_format = {'symbol': self.coins[mess['ch'].split('.')[1]],
                                   'price': trades['price'],
                                   'volume': trades['amount'],
                                   'side': trades['direction'].upper(),
                                   'time': t.isoformat()}
                    data = {"event": "Update", "data": mess_format, "exchange": self.name}
                    self.pub.dispatch('data_feeder', data)
