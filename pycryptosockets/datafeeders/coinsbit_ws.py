import json

from time import sleep
from datetime import (
    datetime as dt,
    tzinfo,
    timedelta,
)

from .._base import BaseWebsocketClient


class SimpleUTC(tzinfo):
    def tzname(self, **kwargs):
        return "UTC"
    def utcoffset(self, dt):
        return timedelta(0)


class CoinsBitWebsocketClient(BaseWebsocketClient):

    def __init__(self, reconnect_on_error, *args, **kwargs):
        self.url = 'wss://coinsbit.io/trade_ws'
        super().__init__(
            name='CoinsBit',
            url=self.url,
            channels=['data_feeder'],
            reconnect_on_error=reconnect_on_error,
            header={
                'content-type': 'application/json-rpc',
                'accept': 'application/json-rpc'
            }
        )

    def subscribe_socket(self, pairs):
        subscribe = {
            "method": "price.subscribe",
            "params": pairs,
            "id": 0,
        }
        self.send_message(subscribe)

    def on_message(self, message):
        try:
            message = json.loads(message)
            if message['method'] == 'state.update':
                if message['params'][0] in self.coins:
                    message_format = dict(
                        symbol=message['params'][0].replace('_', '-'),
                        price=message['params'][1]['last'],
                        volume=message['params'][1]['volume'],
                        side='N/A',
                        time=dt.utcnow().replace(tzinfo=SimpleUTC()).isoformat()
                    )
                    data = {
                        "event": "Update",
                        "data": message_format,
                        "exchange": self.name
                    }
                    self.pub.dispatch('data_feeder', data)
        except Exception as e:
            print(e)
