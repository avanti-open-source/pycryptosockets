import json
import datetime as dt
from .._base import BaseWebsocketClient

class LivecoinWebsocketClient(BaseWebsocketClient):
    """
    Base class to recieve Livecoin Exchange data
    """
    def __init__(self, reconnect_on_error, *args, **kwargs):
        self.url = 'wss://ws.api.livecoin.net/ws/beta2'
        self.coins_dict = {}
        super().__init__('Livecoin', self.url, channels=['data_feeder'],
                         reconnect_on_error=reconnect_on_error)
        
    def subscribe_socket(self, pairs):
        """
        Function to subscribe to the trade data
        """
        for pair in pairs:
            dict_subscribe = {
                "Subscribe": {
                    "channelType" :"trade",
                    "symbol" : pair,
                }
            }
            self.send_message(dict_subscribe)
            
    def on_message(self, msg):
        """
        Custom class to handle messages
        """
        mess = json.loads(msg)
        if msg != "":
            if "operation" not in msg:
                symbol = self.coins_dict[mess['channelId']]
                t = dt.datetime.fromtimestamp(mess['timestamp']/1e3, self.localzone)
                t = t.astimezone(self.utczone)
                data = {'symbol': symbol,
                        'price': abs(mess['price']),
                        'volume': mess['quantity'],
                        'side' : "BUY" if mess['price'] < 0 else "SELL",
                        'time': str(t)}
                mdata = {"event": "Update", "data": data, "exchange": self.name}
                self.pub.dispatch(self.channels[0], mdata)
            else:
                self.coins_dict[mess['channelId']] = self.coins[mess['operation']['Subscribe']['symbol']]
                