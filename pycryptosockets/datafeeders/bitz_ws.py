import json

from loguru import logger
from datetime import datetime as dt
from .._base import BaseWebsocketClient


class BitzWebsocketClient(BaseWebsocketClient):
    """Class to receive and delivery Bitz trades data."""

    def __init__(self, reconnect_on_error, *args, **kwargs):
        self.url = 'wss://wsapi.bitz.top/'
        super().__init__(
            'Bitz',
            self.url,
            channels=['data_feeder'],
            reconnect_on_error=reconnect_on_error,
            ping_interval=8,
        )

    def subscribe_socket(self, pairs):
        for pair in pairs:
            json_subscription = dict(
                action="Topic.sub",
                data=dict(
                    symbol=pair,
                    type="order",
                    _CDID="100002",
                )
            )
            self.send_message(json_subscription)

    def on_message(self, msg):
        
        message = json.loads(msg)

        if message.get('data'):
            symbol = message['params']['symbol'].replace('_', '-').upper()
            for trade in message['data']:
                d_time = dt.fromtimestamp(trade['T'], self.localzone)
                message_format = dict(
                    symbol=symbol,
                    price=float(trade['p']),
                    volume=float(trade['n']),
                    side=trade['s'],
                    time=d_time.isoformat(),
                )

                data = dict(event="Update", data=message_format, exchange=self.name)
                self.pub.dispatch('data_feeder', data)