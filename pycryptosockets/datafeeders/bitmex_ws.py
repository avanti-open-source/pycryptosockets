from .._base import BaseWebsocketClient
import json


class BitmexWebsocketClient(BaseWebsocketClient):
    """Class to receive and deliver Bitmex trades data."""

    def __init__(self, reconnect_on_error, *args, **kwargs):
        """Built method."""
        self.url = 'wss://www.bitmex.com/realtime'
        super().__init__('Bitmex', self.url, channels=['data_feeder'],
                         reconnect_on_error=reconnect_on_error)

    def subscribe_socket(self, pairs):
        """Customize subscription."""
        for coin in pairs:
            dict_ = {"op": "subscribe",
                     "args": "trade:{}".format(coin)
                     }
            self.send_message(dict_)

    def on_message(self, msg):
        """
        May be overriden.

        Handle the message and dispatch it.
        :param msg: String with json format
        """
        mess = json.loads(msg)
        if mess.get('table'):
            if mess['action'] == 'insert':
                for trade in mess['data']:
                    mess_d = {
                        'symbol': trade['symbol'],
                        'price': trade['price'],
                        'volume': trade['homeNotional'],
                        'side': trade['side'],
                        'time': str(trade['timestamp'].replace('Z', '') + "000+00:00")
                    }
                    data = {"event": "Update",
                            "data": mess_d,
                            "exchange": self.name}
                    self.pub.dispatch(self.channels[0], data)
