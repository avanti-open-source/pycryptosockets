import json

from loguru import logger
from datetime import datetime as dt

from .._base import BaseWebsocketClient


class COINBASEPROWebsocketClient(BaseWebsocketClient):

    def __init__(self, reconnect_on_error, *args, **kwargs):
        self.url = 'wss://ws-feed.pro.coinbase.com'
        super().__init__(
            'COINBASEPRO',
            self.url,
            channels=['data_feeder'],
            reconnect_on_error=reconnect_on_error,
        )

    def subscribe_socket(self, pairs):
        subscribe = dict(
            type='subscribe',
            product_ids=pairs,
            channels=[
                dict(
                    name='heartbeat',
                    products_ids=pairs
                ),
                dict(
                    name='full',
                    product_ids=pairs
                ),
            ]
        )
        self.send_message(subscribe)

    def on_message(self, message):
        message_json = json.loads(message)
        try:
            if message_json['type'] == 'match':
                trade_time = message_json['time'].replace('Z', '') + '+00:00'
                message_format = dict(
                    symbol=message_json['product_id'],
                    price=message_json['price'],
                    volume=message_json['size'],
                    side=message_json['side'],
                    time=trade_time,
                )
                data = {
                    "event": "Update",
                    "data": message_format,
                    "exchange": self.name
                }
                self.pub.dispatch('data_feeder', data)
        except KeyError as e:
            logger.error(f"Error in Key: {e}")
