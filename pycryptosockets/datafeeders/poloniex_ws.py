from datetime import datetime as dt
from .._base import BaseWebsocketClient
import requests, json
import time


class PoloniexWebsocketClient(BaseWebsocketClient):
    """Class to receive and deliver Poloniex trades data."""

    def __init__(self, reconnect_on_error, *args, **kwargs):
        """Builder method."""
        self.url = "wss://api2.poloniex.com"
        self.pair_coins = {}
        self.heartbeat = 0
        self.keys = self.get_keys()
        super().__init__('Poloniex', self.url, channels=['data_feeder'],
                         reconnect_on_error=reconnect_on_error)

    def get_keys(self):
        """
        Get currency pairs of Poloniex from the api.

        :return : key(). value
        """
        t = {}
        url = 'https://poloniex.com/public?command=returnTicker'  # tickers
        response = requests.get(url).text
        keys = json.loads(response)
        for a, b in keys.items():
            t[b['id']] = a
        return t

    def subscribe_socket(self, pairs):
        """Custom subscription."""
        for coin in pairs:
            dict_ = {'command': 'subscribe',
                     'channel': coin}
            self.send_message(dict_)

    def on_message(self, msg):
        """
        May be overriden.

        Handle the message and dispatch it.
        :param msg: String with json format
        """
        mess = json.loads(msg)
        if 't' in msg:
            list_ = mess[2]
            for i in list_:
                if 't' in i:
                    sym = self.coins[self.keys.get(mess[0])]
                    t = dt.fromtimestamp(i[-1], self.localzone)
                    t = t.astimezone(self.utczone)
                    message = {'symbol': sym,
                               'price': float(i[3]),
                               'volume': float(i[4]),
                               'side': 'BUY' if i[2] == 1 else 'SELL',
                               'time': t.isoformat()}

                    data = {"event": "Update", "data": message, "exchange": self.name}
                    self.pub.dispatch(self.channels[0], data)
                else:
                    self.heartbeat += 1
                    if self.heartbeat == 35:
                        data = {"event": "Heartbeat", "exchange": self.name}
                        self.pub.dispatch('data_feeder', data)
                        self.heartbeat = 0
        else:
            self.heartbeat += 1
            if self.heartbeat == 35:
                data = {"event": "Heartbeat", "exchange": self.name}
                self.pub.dispatch('data_feeder', data)
                self.heartbeat = 0
