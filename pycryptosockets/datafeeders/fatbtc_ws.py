import json
# import gzip
# import base64
import requests

from datetime import datetime as dt
from .._base import BaseWebsocketClient


class FatBTCWebsocketClient(BaseWebsocketClient):
    """
    Class to receive and deliver FatBTC trades data.
    """

    def __init__(self, reconnect_on_error, *args, **kwargs):
        self.url = 'wss://www.fatbtc.us/websocket'
        super().__init__(
            'FatBTC',
            self.url,
            channels=['data_feeder'],
            reconnect_on_error=reconnect_on_error,
        )

    def obtain_timestamp(self):
        str_data = requests.get('https://www.fatbtc.us/m/timestamp/timestamp').text
        json_data = json.loads(str_data)
        return json_data['data']

    def subscribe_socket(self, pairs):
        """
        Subscriptions for the currencies available in the exchange of FatBTC.
        :param pairs: list of coin pairs.
        :return:
        """
        for pair in pairs:
            subscribe = dict(
                key=f'trades_{pair}_10',
                ts=self.obtain_timestamp()
            )
            self.send_message(subscribe)

    def return_base_quote(self, coin):
        coin = coin.upper()
        quotes = ['USDT', 'FCNY', 'BGBP', 'USDC', 'GUSD', 'USDK']
        if coin[-4:] in quotes:
            pair = coin[:-4] + '-' + coin[-4:]
        else:
            pair = coin[:-3] + '-' + coin[-3:]
        return pair

    def on_message(self, msg):
        """
        Custom on_message.
        :param msg: data of the currency to which you have subscribed.
        :return:
        """

        message = json.loads(msg)
        if message.get('data'):
            data_trades = message['data']['trades']
            symbol = self.return_base_quote(message['data']['symbol'])
            for trade in data_trades:
                trade_datetime = dt.fromtimestamp(trade['timestamp']/1e3, self.localzone)
                trade_datetime = trade_datetime.astimezone(self.utczone)
                message_format = dict(
                    symbol=symbol,
                    price=trade['price'],
                    volume=trade['volume'],
                    side=trade['taker'],
                    time=trade_datetime.isoformat(),
                )

                data = {
                    "event": "Update",
                    "data": message_format,
                    "exchange": self.name
                }
                self.pub.dispatch('data_feeder', data)
