from .._base import BaseWebsocketClient
import json
import time
import datetime as dt


class GDAXWebsocketClient(BaseWebsocketClient):
    """
    Class to recive and deliver GDAX trades data
    """

    def __init__(self, reconnect_on_error, *args, **kwargs):
        self.url = 'wss://ws-feed.gdax.com'
        self.heartbeats = 0
        self.msg_heartbeat = 120
        super().__init__('GDAX', self.url, channels=['data_feeder'],
                         reconnect_on_error=reconnect_on_error)

    def subscribe_socket(self, pairs):
        """
        subscriptions for the currencies available in the exchange of GDAX.
        :param pairs: list of coin pairs.
        :return:
        """

        if type(pairs) is list:
            # if there are more than two pairs of coins
            request = {"type": "subscribe", "product_ids": pairs}
        else:
            request = {"type": "subscribe", "product_id": pairs}
        self.send_message(request)

    def on_message(self, msg):
        """
        Custom on_message.
        :param msg: data of the currency to which you have subscribed.
        :return:
        """
        mess = json.loads(msg)
        if mess['type'] == 'match':
            symbol = self.coins[mess['product_id']]
            mess_format = {'symbol': symbol,
                           'price': float(mess['price']),
                           'volume': float(mess['size']) if 'size' in mess else float(mess['remaining_size']),
                           'side': mess['side'].upper(),
                           'time': mess['time'].replace('Z', '') + "+00:00"}

            data = {"event": "Update", "data": mess_format, "exchange": self.name}
            self.pub.dispatch('data_feeder', data)
        self.heartbeats += 1
        if self.heartbeats == self.msg_heartbeat:
            data = {"event": "Heartbeat", "exchange":self.name}
            self.pub.dispatch('data_feeder', data)
            self.heartbeats = 0
