import datetime as dt

from pubnub.callbacks import SubscribeCallback
from pubnub.pubnub import PubNub, SubscribeListener
from pubnub.enums import PNOperationType, PNStatusCategory
from pubnub.pnconfiguration import PNConfiguration
from .._base import BaseWebsocketClient


class MySubscribeCallback(SubscribeCallback):

    def presence(self, pubnub, presence):
        # print("presence, ", presence)  # handle incoming presence data
        pass

    def message(self, pubnub, message):
        # print("message: ", message.message, 'channel: ', message.channel)  # handle incoming messages
        # message, subscription, channel, timetoken
        pass


class BitbankWebsocketClient(BaseWebsocketClient, MySubscribeCallback):
    """websocket for bitbank exchange."""

    def __init__(self, reconnect_on_error, *args, **kwargs):
        """Builder method."""
        self.url = ''
        self.multi = []
        key = kwargs['kwargs']['Bitbank']['subscribe_key']
        self.pnconfig = PNConfiguration()
        self.pnconfig.subscribe_key = key
        self.channels = ['data_feeder']
        self.pubn = PubNub(self.pnconfig)
        super().__init__(
            'Bitbank',
            self.url,
            channels=self.channels,
            reconnect_on_error=reconnect_on_error,
        )

    def start_connection(self):
        """Client that connects when listen."""
        pass

    def on_message(self, msg, sym):
        """
        May be overriden.

        Handle the message and dispatch it.
        :param msg: String with json format
        """
        symbol = self.coins[sym.replace('transactions_', '')]
        for data in msg['data']['transactions']:
            t = dt.datetime.fromtimestamp(data['executed_at']/1000)
            t = t.replace(tzinfo=self.localzone).astimezone(self.utczone)
            msg = {'symbol': symbol,
                   'price': float(data['price']),
                   'side': data['side'],
                   'time': str(t)}
            data = {'event': 'Update', 'data': msg, 'exchange': self.name}
            self.pub.dispatch(self.channels[0], data)

    def message(self, pubnub, message):
        self.on_message(message.message, message.channel)

    def subscribe_socket(self, pairs):
        """
        Method that return a list of pairs.

        :param pairs: 'transactions_btc_jpy'
        """
        ticker = pairs
        for coin in ticker:
            self.multi.append('transactions_' + coin)
        self.pubn.add_listener(self)

    def listen(self):
        """
        Main function to recieve messages.

        Should be on a thread
        """
        a = self.pubn.subscribe().channels(self.multi).execute()
        self.pub.dispatch(self.channels[0], a)

    def close_connection(self):
        self.pubn.remove_listener(self)
