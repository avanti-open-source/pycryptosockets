from .._base import BaseWebsocketClient
from websocket import create_connection
from threading import Thread
import time
from datetime import datetime as dt, timezone
import json
import pytz
from dateutil import parser
import zlib


def inflate(data):
    """Function for unzip messages form okex."""
    decompress = zlib.decompressobj(
            -zlib.MAX_WBITS  # see above
    )
    inflated = decompress.decompress(data)
    inflated += decompress.flush()
    return inflated


class OkexWebsocketClient(BaseWebsocketClient):
    """
    Class that receives and delivers data from Okex.
    """
    def __init__(self, reconnect_on_error, *args, **kwargs):
        self.url = 'wss://real.okex.com:8443/ws/v3'
        self.count = 0
        super().__init__(
            'Okex',
            self.url,
            channels=['data_feeder'],
            reconnect_on_error=reconnect_on_error,
            ping_interval=15,
        )

    def start_connection(self):
        self._ws = create_connection(self.url)
        t = Thread(target=self.ping_server)
        t.start()

    def subscribe_socket(self, pairs):
        """
        subscriptions for the currencies available in the exchange of Okex.
        :param pairs: list of coin pairs .
        :return:
        """
        for coin in pairs:
            request = {
                'op': 'subscribe',
                'args': ['spot/trade:' + coin],
            }
            self.send_message(request)

    def on_message(self, msg):
        """
        Custom on_message.
        :param msg: data of the currency to which you have subscribed.
        :return:
        """
        msg = inflate(msg)
        mess = b'{}'.decode('utf-8').format(msg)
        mess = json.loads(msg.decode())
        if mess.get('table') and self.count > len(self.coins):

            for trade in mess['data']:
                mess_format = {
                    'symbol': trade['instrument_id'],
                    'price': float(trade['price']),
                    'volume': float(trade['size']),
                    'side': 'N/A',
                    'time': trade['timestamp'],
                }

                data = {"event": "Update", "data": mess_format, "exchange": self.name}
                self.pub.dispatch('data_feeder', data)
        else:
            self.count += 1

    def ping_server(self):
        while self.go_on:
            time.sleep(self.ping)
            data = {
                "event": "Heartbeat",
                "exchange":self.name
            }
            self.pub.dispatch('data_feeder', data)
            self._ws.ping()
