import json
import datetime as dt

from .._base import BaseWebsocketClient


class KrakenWebsocketClient(BaseWebsocketClient):
    """Class to recive and deliver Kraken trades data."""

    def __init__(self, reconnect_on_error, *args, **kwargs):
        """Builder method."""
        self.url = 'wss://ws.kraken.com'
        self.heartbeats = 0
        self.side = {'b': 'buy', 's': 'sell'}
        super().__init__('Kraken', self.url, channels=['data_feeder'],
                         reconnect_on_error=reconnect_on_error)

    def subscribe_socket(self, pairs):
        """Custom subscription."""
        dict_ = {
            "event": "subscribe",
            "pair": pairs,
            "subscription": {
                "name": "trade",
            }
        }
        self.send_message(dict_)

    def on_message(self, msg):
        message = json.loads(msg)
        if isinstance(message, list):
            symbol = message[-1]
            for trade in message[1]:
                dtime = dt.datetime.fromtimestamp(float(trade[2]), self.localzone)
                dtime = dtime.astimezone(self.utczone)
                message = {
                    'symbol': symbol.replace("/", "-"),
                    'price': float(trade[0]),
                    'volume': float(trade[1]),
                    'side': self.side[trade[3]],
                    'time': dtime.isoformat(),
                }
                data = {"event": "Update", "data": message, "exchange": self.name}
                self.pub.dispatch('data_feeder', data)
