import json
import datetime as dt

from .._base import BaseWebsocketClient


class GateIOWebsocketClient(BaseWebsocketClient):
    """Class  to recive and deliver Gate.IO data."""

    def __init__(self, reconnect_on_error, *args, **kwargs):
        """Initializer"""
        self.url = 'wss://ws.gate.io/v3/'
        self.count = 0
        super().__init__('GateIO', self.url, channels=['data_feeder'],
                         reconnect_on_error=reconnect_on_error)

    def subscribe_socket(self, pairs):
        sub_msg = {'id':12312, 'method':'trades.subscribe', 'params':pairs}
        self.send_message(sub_msg)

    def on_message(self, msg):
        if self.count > len(self.coins):
            data = json.loads(msg)
            if 'method' in data:
                if data['method'] == "trades.update":
                    symbol, trades = data['params']
                    for trade in trades:
                        sym = self.coins[symbol]
                        t = dt.datetime.fromtimestamp(trade['time'], self.localzone)
                        t = t.astimezone(self.utczone)
                        mess_d = {
                                'symbol': sym,
                                'price' : float(trade['price']),
                                'volume': float(trade['amount']),
                                'side': trade['type'].upper(),
                                'time': t.isoformat()
                            }
                        mdata = {"event": "Update", "data": mess_d, "exchange": self.name}
                        self.pub.dispatch('data_feeder', mdata)
        else:
            self.count += 1
