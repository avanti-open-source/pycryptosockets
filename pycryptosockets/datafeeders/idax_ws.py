import json

from datetime import datetime as dt
from .._base import BaseWebsocketClient


class IDAXWebsocketClient(BaseWebsocketClient):
    """Class to receive and deliver Iadx trades data."""

    def __init__(self, reconnect_on_error, *args, **kwargs):
        self.url = 'wss://openws.idax.pro/ws'
        super().__init__(
            'IDAX',
            self.url,
            channels=['data_feeder'],
            reconnect_on_error=reconnect_on_error,
            ping_interval=5,
        )

    def subscribe_socket(self, pairs):
        bulk_subscripcion = list()
        for coin in pairs:
            json_subscripcion = dict(
                event='addChannel',
                channel='idax_sub_{}_trades'.format(coin),
            )
            bulk_subscripcion.append(json_subscripcion)
        self.send_message(bulk_subscripcion)

    def on_message(self, msg):

        message = json.loads(msg)
        if message['data']:
            channel = message['channel'].upper().split('_')
            symbol = channel[2] + '-' + channel[-2]
            for trade in message['data']:
                dtime = dt.fromtimestamp(trade[3]/1e3, self.localzone)
                message_format = dict(
                    symbol=symbol,
                    price=float(trade[1]),
                    volume=float(trade[2]),
                    side=trade[-1],
                    time=dtime.isoformat(),
                )
                data = dict(event="Update", data=message_format, exchange=self.name)
                self.pub.dispatch('data_feeder', data)