import json
import datetime as dt

from .._base import BaseWebsocketClient


class BitstampWebsocketClient(BaseWebsocketClient):
    """Class to recive and deliver Bitstamp trades data."""
    def __init__(self, reconnect_on_error, *args, **kwargs):
        """Builder method."""
        self.url = "wss://ws.bitstamp.net"
        super().__init__(
            'Bitstamp',
            self.url,
            channels=['data_feeder'],
            reconnect_on_error=reconnect_on_error,
            ping_interval=15,
        )
        self.buybtcusd = None
        self.sellbtcusd = None

    def subscribe_socket(self, pairs):
        """Custom subscription."""
        for pair in pairs:
            json_subscription = {
                "event": "bts:subscribe",
                "data": {"channel": "live_trades_" + pair},
            }
            self.send_message(json_subscription)

    def on_message(self, msg):
        dict_message = json.loads(msg)
        if dict_message['event'] == 'trade':
            data_trade = dict_message['data']
            channel = dict_message['channel'].split("_")

            if not isinstance(dict_message, dict):
                data_trade = json.loads(dict_message['data'])

            datetime_zone = dt.datetime.fromtimestamp(int(data_trade['timestamp']), self.localzone)
            datetime_utc_zone = datetime_zone.astimezone(self.utczone)
            dict_message = {
                'symbol': self.coins[channel[-1]],
                'price': data_trade['price'],
                'volume': data_trade['amount'],
                'side': 'buy' if data_trade['type'] == 0 else 'sell',
                'time': datetime_utc_zone.isoformat(),
            }

            data = {
                "event": "Update",
                "data": dict_message,
                "exchange": self.name,
            }

            if dict_message['symbol'] == 'BTC-USD':
                if dict_message['side'] == 'buy':
                    self.buybtcusd = dict_message['price']
                elif dict_message['side'] == 'sell':
                    self.sellbtcusd = dict_message['price']

                if self.buybtcusd and self.sellbtcusd:
                    dict_message['price'] = (self.buybtcusd + self.sellbtcusd)/2
                    print(self.buybtcusd, '-', self.sellbtcusd, '=', dict_message['price'])
                    self.pub.dispatch('data_feeder', data)
            else:
                self.pub.dispatch('data_feeder', data)
