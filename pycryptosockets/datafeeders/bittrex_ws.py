from time import sleep
from datetime import datetime
from .._base import BaseWebsocketClient
from bittrex_websocket.websocket_client import BittrexSocket


class BittrexWebsocketClient(BittrexSocket, BaseWebsocketClient):

    def __init__(self, reconnect_on_error, url=None, *args, **kwargs):
        BaseWebsocketClient.__init__(self, name='Bittrex', url=None, channels=['data_feeder'],
                                     reconnect_on_error=reconnect_on_error, *args, **kwargs)

    def start_connection(self):
        BittrexSocket.__init__(self, url=None)

    def subscribe_socket(self, pairs):
        """Subscribes to the stream of a list of pairs
        :param: pairs. Type: list. pairs of the subscription.
        """

        # Subscribe to ticker stream
        for ticker in pairs:
            sleep(0.05)
            super().subscribe_to_exchange_deltas([ticker])

    def on_public(self, msg):
        """Handler of every trade message.
                :param: msg: dict.
        JSON Keys
        "f" = "Fills", "Z" = "Buys", "TY" = "Type"
        "S" = "Sells", "R" = "Rate", "Q" = "Quantity"
        "q" = "QuantityRemaining", "T" = "TimeStamp",
        "N" = "Nonce", "M" = "MarketName"
        """

        sym = self.coins[msg['M']]

        for trade in msg['f']:
            t = datetime.fromtimestamp(trade['T'] / 1e3, self.localzone)
            t = t.astimezone(self.utczone)
            mess = {
                'symbol': sym,
                'price': trade['R'],
                'volume': trade['Q'],
                'side': trade['OT'],
                'time': t.isoformat()
            }
            data = {"event": "Update", "data": mess, "exchange": self.name}
            self.pub.dispatch('data_feeder', data)

    def listen(self):
        while True:
            try:
                sleep(1)
            except KeyboardInterrupt:
                super().disconnect()
                break

    def close_connection(self):
        """
        Closes connection to the server
        """
        super().disconnect()