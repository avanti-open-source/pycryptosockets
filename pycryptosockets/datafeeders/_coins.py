import requests
import json

# pairs : pycrypto format pair
# symbols : subscription format symbol

def binance_coins():
    request_binance_api = requests.get('https://api.binance.com/api/v3/exchangeInfo').text
    data_symbols = json.loads(request_binance_api)['symbols']
    pairs = [f"{data['baseAsset']}-{data['quoteAsset']}" for data in data_symbols]
    symbols = [data['symbol'] for data in data_symbols]
    return pairs, symbols

def bitfinex_coins():
    """
    Get the currency pairs of the Bitfinex from the api rest.

    :return: list with the currency pairs and the symbols..
    """
    short_names = {
        'iot': 'iota',
        'dsh': 'dash',
        'qtm': 'qtum',
        'qsh': 'qash',
        'dat': 'data'
    }

    url = 'https://api.bitfinex.com/v1/symbols'  # pairs
    response = requests.get(url)
    api_pairs = response.json()
    pairs = [f"{short_names.get(pair[:3], pair[:3])}-{short_names.get(pair[-3:], pair[-3:])}".upper() for pair in api_pairs]
    symbols = api_pairs

    return pairs, symbols


def bitstamp_coins():
    """
    Get the currency pairs of the Bitstamp from the api rest.

    :return: list with the currency pairs and the symbols..
    """
    r = requests.get("https://www.bitstamp.net/api/v2/trading-pairs-info/")
    data = r.json()
    pairs = [d['name'].replace("/","-") for d in data]
    symbols = [d['url_symbol'] for d in data]
    return pairs, symbols


def gdax_coins():
    """
    Get the currency pairs of the GDAX from the api rest.

    :return: list with the currency pairs and the symbols..
    """
    url = 'https://api-public.sandbox.gdax.com/'  # pairs
    request = ['products', 'currencies']
    response = requests.get(url + request[0])
    js = response.json()
    pairs = [dicts['id'] for dicts in js]
    return pairs, pairs


def okex_coins():
    """
    Get the currency pairs of the Okex from the api rest.

    :return: list with the currency pairs and the symbols..
    """

    url = requests.get('https://www.okex.com/api/spot/v3/instruments')
    request = url.json()
    pairs = [instrument['instrument_id'] for instrument in request]
    symbols = [instrument['instrument_id'] for instrument in request]
    return pairs, symbols


def poloniex_coins():
    """
    Get the currency pairs of the Poloniex from the api rest.

    :return: list with the currency pairs and the symbols..
    """

    url = 'https://poloniex.com/public?command=returnTicker'  # tickers
    response = requests.get(url).text
    keys = json.loads(response)
    symbols = list(keys.keys())
    pairs = ["{}-{}".format(i.split("_")[1], i.split("_")[0]) for i in symbols]

    return pairs, symbols


def hitbtc_coins():
    """
    Get the currency pairs of the HitBTC from the api rest.

    :return: list with the currency pairs and the symbols.
    """
    url = 'https://api.hitbtc.com/api/2/public/'  # pairs
    request = ['symbol', 'currencies']
    response = requests.get(url + request[0])
    js = response.json()
    pairs = ["{}-{}".format(dicts['baseCurrency'], dicts['id'].replace(dicts['baseCurrency'],"")) for dicts in js]
    symbols = [dicts['id'] for dicts in js]

    return pairs, symbols


def bittrex_coins():
    """
    Get the currency pairs of the bittrex from the coinmarketcap.

    :return: list with the currency pairs and the symbols..
    """
    r = requests.get("https://bittrex.com/api/v1.1/public/getmarkets")
    data = r.json()['result']
    pairs = ["{}-{}".format(i['MarketName'].split("-")[1], i['MarketName'].split("-")[0])
                for i in data if i['IsActive']]
    symbols = [i['MarketName'] for i in data if i['IsActive']]
    return pairs, symbols


def bitbank_coins():
    """
    Get the currency pairs of the Bitbank from the coinmarketcap.

    :return: list with the currency pairs and the symbols..
    """
    requests_assets = requests.get('https://api.bitbank.cc/v1/spot/pairs').text
    assets_dict = json.loads(requests_assets)['data']
    symbols = [pair['name'] for pair in assets_dict['pairs']]
    pairs = [altcoin.split('_')[0].upper() + '-' + altcoin.split('_')[-1].upper() for altcoin in symbols]
    return pairs, symbols


def huobipro_coins():
    """
    Get the currency pairs of the Huobipro from the api rest.

    :return: list with the currency pairs and the symbols..
    """
    url = 'https://api.huobi.pro/v1/common/symbols'
    res = requests.get(url)
    js = res.json()

    pairs = [(pair['base-currency'] + '-' + pair['quote-currency']).upper() for pair in js['data']]
    symbol = [pair['base-currency'] + pair['quote-currency'] for pair in js['data']]

    return pairs, symbol


def gateio_coins():
    r = requests.get("https://data.gate.io/api2/1/pairs")
    data = r.json()
    pairs = ["{}-{}".format(coin.split("_")[0], coin.split("_")[1]) for coin in data]
    return pairs, data


def livecoin_coins():
    r = requests.get("https://api.livecoin.net/exchange/ticker")
    data = r.json()
    pairs = [x['symbol'].replace("/","-") for x in data]
    symbols = [x['symbol'] for x in data]
    return pairs, symbols


def bitflyer_coins():
    end_point = ['/', '/usa', '/eu']
    symbols = []
    pairs = []
    for end in end_point:
        r = requests.get('https://api.bitflyer.jp/v1/getmarkets' + end)
        data = r.json()
        symbols += [pair['product_code'] for pair in data if len(pair['product_code'].split('_')) < 3 and
                    'alias' not in pair]
        pairs += [pair['product_code'].replace('_', '-') for pair in data if len(pair['product_code'].split('_')) < 3 and
                  'alias' not in pair]

    return pairs, symbols


def zaif_coins():
    url = "https://api.zaif.jp/api/1/currency_pairs/all"
    r = requests.get(url)
    data = r.json()
    pairs = [x['name'].replace("/", "-") for x in data]
    symbols = [x['currency_pair'] for x in data]
    return pairs, symbols


def kucoin_coins():
    """Return all pairs in kucoin."""
    request = requests.get('https://api.kucoin.com/api/v1/symbols')
    data = request.json()
    pairs = [ku['symbol'] for ku in data['data']]
    symbols = [ku['symbol'] for ku in data['data']]
    return pairs, symbols


def bitmex_coins():
    """Return all pairs in Bitmex."""
    r = requests.get("https://www.bitmex.com/api/v1/instrument/active")
    j = json.loads(r.text)
    j = [{'market_name': item['symbol'], 'base': item['quoteCurrency'],
          'ticker': item['rootSymbol']}
         for item in j if not item['rootSymbol'] == item['quoteCurrency']]
    # pairs = [i['base'] + '-' + i['ticker'] for i in j]
    pairs = [i['market_name'] for i in j]
    symbols = [i['market_name'] for i in j]
    return pairs, symbols


def bitmax_coins():
    """Return all pairs in Bitmax."""
    request_bitmax_products = requests.get('https://bitmax.io/api/v1/products').text
    json_bitmax_assets = json.loads(request_bitmax_products)
    symbols = [asset['symbol'] for asset in json_bitmax_assets]
    pairs = [symbol.replace('/', '-') for symbol in symbols]
    return pairs, pairs


def oex_coins():
    """Return all pairs in OEX."""
    request = requests.get('https://openapi.oex.com/open/api/common/symbols').text
    dict_oex_assets = json.loads(request)
    symbols = [data_currency['symbol'] for data_currency in dict_oex_assets['data']]
    pairs = [
        data_currency['base_coin'] + '-' + data_currency['count_coin'] 
        for data_currency in dict_oex_assets['data']
    ]
    return pairs, symbols


def cointiger_coins():
    """Return all pairs in cointiger"""
    request = requests.get('https://www.cointiger.com/exchange/api/public/market/detail').text
    dict_cointiger_assets = json.loads(request)
    symbols = list(dict_cointiger_assets.keys())
    pairs = []
    for pair in symbols:
        if pair[-4:] == 'USDT':
            _pair = pairs.append(pair[:-4] + '-' + pair[-4:])
        elif pair[-6:] == 'BITCNY':
            _pair = pairs.append(pair[:-6] + '-' + pair[-6:])
        else:
            _pair = pairs.append(pair[:-3] + '-' + pair[-3:])
    return pairs, symbols


def bibox_coins():
    """Return all pairs in bibox."""
    request = requests.get('https://api.bibox.com/v1/mdata?cmd=pairList').text
    dict_bibox_pairs = json.loads(request)
    symbols = [pair['pair'] for pair in dict_bibox_pairs['result']]
    pairs = [pair.split('_')[0] + '-' + pair.split('_')[-1] for pair in symbols]
    return pairs, symbols


def fatbtc_coins():
    """Return all pairs in fatbtc."""
    request = requests.get('https://www.fatbtc.us/m/symbols/1/timestamp').text
    dict_fatbtc_pairs = json.loads(request)['symbols']
    symbols = [alt['symbol'] for alt in dict_fatbtc_pairs]
    pairs = [alt['base_currency'] + '-' + alt['quote_currency'] for alt in dict_fatbtc_pairs]
    return pairs, symbols


def bitz_coins():
    """Return all pairs in bitz."""
    request = requests.get('https://api.bitzapi.com/Market/tickerall').text
    json_bitz_pairs = json.loads(request)
    symbols = list(json_bitz_pairs['data'].keys())
    pairs = [symbol.replace('_', '-').upper() for symbol in symbols]
    return pairs, symbols


def idax_coins():
    """Return all pairs in idax."""
    request = requests.get('https://openapi.idax.pro/api/v2/pairs').text
    json_idax_pairs = json.loads(request)
    symbols = list(map(lambda pair: pair.lower(), json_idax_pairs['pairs']))
    pairs = [pair.replace('_', '-').upper() for pair in symbols]
    return pairs, symbols


def coinbasepro_coins():
    request_url = requests.get('https://api.pro.coinbase.com/products').text
    json_coinbase_pairs = json.loads(request_url)
    symbols = [currency['id'] for currency in json_coinbase_pairs]
    pairs = symbols
    return pairs, symbols


def p2pb2b_coins():
    request_url = requests.get('https://api.p2pb2b.io/api/v1/public/markets').text
    json_p2pb2b = json.loads(request_url)['result']
    symbols = [currency['name'] for currency in json_p2pb2b]
    pairs = [currency.replace('_', '-') for currency in symbols]
    return pairs, symbols


def coinsbit_coins():
    request_url = requests.get('https://coinsbit.io/api/v1/public/symbols').text
    json_coinsbit = json.loads(request_url)['result']
    pairs = [pair.replace('_', '-') for pair in json_coinsbit]
    return pairs, json_coinsbit


def kraken_coins():
    request_url = requests.get('https://api.kraken.com/0/public/AssetPairs').text
    json_kraken_pairs = json.loads(request_url)['result']
    symbols = list()
    for key, value in json_kraken_pairs.items():
        if value.get('wsname'):
            symbols.append(value['wsname'])
    pairs = [pair.replace('/', '-') for pair in symbols]
    return pairs, symbols
