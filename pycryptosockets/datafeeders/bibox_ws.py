import json
import gzip
import base64

from datetime import datetime as dt
from .._base import BaseWebsocketClient


class BiboxWebsocketClient(BaseWebsocketClient):
    """
    Class to receive and deliver Bibox trades data.
    """

    def __init__(self, reconnect_on_error, *args, **kwargs):
        self.url = 'wss://push.bibox.com/'
        super().__init__(
            'Bibox',
            self.url,
            channels=['data_feeder'],
            reconnect_on_error=reconnect_on_error,
        )

    def subscribe_socket(self, pairs):
        """
        Subscriptions for the currencies available in the exchange of Huobipro.
        :param pairs: list of coin pairs.
        :return:
        """
        for pair in pairs:
            subscribe = dict(
                event='addChannel',
                channel=f'bibox_sub_spot_{pair}_deals'
            )
            self.send_message(subscribe)

    def on_message(self, msg):
        """
        Custom on_message.
        :param msg: data of the currency to which you have subscribed.
        :return:
        """
        side = {'1': 'buy', '2':'sell'}
        message = json.loads(msg)
        if isinstance(message, list):
            message = message[0]
            if message.get('data_type') == 1:
                data_decoded = json.loads(gzip.decompress(base64.b64decode(message['data'])).decode('utf-8'))[0]
                symbol = data_decoded['pair'].replace('_', '-')
                trade_datetime = dt.fromtimestamp(data_decoded['time']/1e3, self.localzone)
                message_format = dict(
                    symbol=symbol,
                    price=data_decoded['price'],
                    volume=data_decoded['amount'],
                    side=side[str(data_decoded['side'])],
                    time=trade_datetime.isoformat()
                )

                data = {
                    "event": "Update",
                    "data": message_format,
                    "exchange": self.name
                }
                self.pub.dispatch('data_feeder', data)

        if message.get('ping'):
            heartbeat = dict(pong=message['ping'])
            self.send_message(heartbeat)
