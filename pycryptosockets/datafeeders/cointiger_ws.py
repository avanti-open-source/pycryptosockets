import json
import gzip

from datetime import datetime as dt
from .._base import BaseWebsocketClient


class CoinTigerWebsocketClient(BaseWebsocketClient):
    """
    Class to receive and deliver CoinTiger trades data.
    """

    def __init__(self, reconnect_on_error, *args, **kwargs):
        self.url = 'wss://api.cointiger.com/exchange-market/ws'
        super().__init__(
            'CoinTiger',
            self.url,
            channels=['data_feeder'],
            reconnect_on_error=reconnect_on_error,
        )

    def subscribe_socket(self, pairs):
        """
        Subscriptions for the currencies available in the exchange of Huobipro.
        :param pairs: list of coin pairs.
        :return:
        """
        for pair in pairs:
            pair = pair.lower()
            subscribe = dict(
                event='sub',
                params=dict(
                    channel='market_{}_trade_ticker'.format(pair),
                    id='trade_ticker-{}'.format(pair)
                )
            )
            self.send_message(subscribe)

    def return_base_quote(self, coin):
        if coin[-4:] == 'USDT':
            pair = coin[:-4] + '-' + coin[-4:]
        elif coin[-6:] == 'BITCNY':
            pair = coin[:-6] + '-' + coin[-6:]
        else:
            pair = coin[:-3] + '-' + coin[-3:]
        return pair

    def on_message(self, msg):
        """
        Custom on_message.
        :param msg: data of the currency to which you have subscribed.
        :return:
        """
        decode_message = gzip.decompress(msg).decode('utf-8')
        message = json.loads(decode_message)

        if message.get('ping'):
            heartbeat = dict(pong=message['ping'])
            self.send_message(heartbeat)

        elif message.get('tick'):
            if message['tick']['data']:
                symbol = self.return_base_quote(message['channel'].split('_')[1].upper())
                for trade in message['tick']['data']:
                    trade_datetime = dt.fromtimestamp(trade['ts']/1e3, self.localzone)
                    trade_datetime = trade_datetime.astimezone(self.utczone)
                    message_format = dict(
                        symbol=symbol,
                        price=trade['price'],
                        volume=trade['vol'],
                        side=trade['side'],
                        time=trade_datetime.isoformat()
                    )

                data = {
                    "event": "Update",
                    "data": message_format,
                    "exchange": self.name
                }
                self.pub.dispatch('data_feeder', data)



        