import json
import datetime as dt

from .._base import BaseWebsocketClient
from websocket import create_connection


class BitmaxWebsocketClient(BaseWebsocketClient):
    """Class to recive and deliver Bitmax trades data."""

    def __init__(self, reconnect_on_error, *args, **kwargs):
        """Builder method."""
        self.url = 'wss://bitmax.io/3/api/stream/'
        self.coins_dict = {}
        self.heartbeats = 0
        super().__init__('Bitmax', self.url, channels=['data_feeder'],
                         reconnect_on_error=reconnect_on_error)

    def subscribe_socket(self, pairs):
        """Custom subscription."""
        for coin in pairs:
            self._ws = create_connection(self.url + coin)
            dict_ = {
                'messageType': "subscribe",
                'marketDepthLevel': 20,
                'recentTradeMaxCount': 20,
            }
            self.send_message(dict_)

    def on_message(self, msg):
        mess = json.loads(msg)
        if isinstance(mess, dict):
            if mess['m'] == 'marketTrades':
                print(mess)
                data = {"event": "Update", "data": mess, "exchange": self.name}
                self.pub.dispatch('data_feeder', data)
            else:
                self.heartbeats += 1
                if self.heartbeats == len(self.coins_dict):
                    data = {"event": "Heartbeat", "exchange":self.name}
                    self.pub.dispatch('data_feeder', data)
                    self.heartbeats = 0
