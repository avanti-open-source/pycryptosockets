import json
import time
import os
import pytz

from loguru import logger
from tzlocal import get_localzone
from websocket import create_connection
from websocket._exceptions import WebSocketConnectionClosedException
from websocket._exceptions import WebSocketBadStatusException
from ssl import SSLError
from .utils.pub_sub import Publisher


class BaseWebsocketClient:
    """
    Base class to connect to a websocket server and handle data.

    All its methods may be overriden
    """

    def __init__(self, name='', url='', channels=['default'],
                 reconnect_on_error=True, auth=None, subscribe_message=None,
                 header=None, ping_interval=30, *args, **kwargs):
        """
        Initializer.

        :param url: url address of the websocket server
        :param channels: list of publication channels
        :param subscribe_message: message to send to the server for subscribe to data and auth
        :param header: dict or list headers to send with the connection for auth or subscription
        :param ping_interval: seconds between ping messages to the server
        """
        self.url = url
        self.name = name
        self.channels = channels
        self.pub = Publisher(channels)
        self.subscribe = subscribe_message
        self.header = header
        self.ping = ping_interval
        self.reconnect_on_error = reconnect_on_error
        self.localzone = get_localzone()
        self.utczone = pytz.UTC
        self.go_on = True
        self.coins = {}

    def start_connection(self):
        """
        May be overriden.

        Connect to the server
        """
        if self.header:
            self._ws = create_connection(self.url, header=self.header)
        else:
            self._ws = create_connection(self.url)
        if self.subscribe:
            self._ws.send(json.dumps(self.subscribe))

    def listen(self):
        """
        Main function to recieve messages.

        Should be on a thread
        """
        while self.go_on:
            try:
                if int(time.time()) % self.ping == 0:
                    self._ws.ping()
                msg = self._ws.recv()
                self.on_message(msg)
            except (ConnectionResetError, SSLError,
                    ConnectionAbortedError, BrokenPipeError,
                    WebSocketConnectionClosedException,
                    TimeoutError, WebSocketBadStatusException) as e:
                self.close_connection()
                self.on_error(e)
            except (ValueError, IndexError) as e:
                logger.error(f"{self.name}, {e.__class__.__name__}")
                self.on_error(e)
                self.close_connection()

    def send_message(self, msg):
        """
        Send a message to the websocket server.

        For extra subscriptions or unsubscribe
        :param msg: dictionary with message format
        """
        self._ws.send(json.dumps(msg))

    def on_message(self, msg):
        """
        May be overriden.

        Handle the message and dispatch it.
        :param msg: String with json format
        """
        self.pub.dispatch(self.channels[0], msg)

    def on_error(self, e):
        """
        Handle error
        :param e: Exception that happend
        """
        name = str(e)
        self._ws = "Error on {}: {}".format(self.name, name)
        logger.warning("#"*60)
        logger.warning(self._ws)
        logger.warning("#"*60)
        if self.reconnect_on_error:
            time.sleep(1)
            self.go_on = True
            self.start_connection()
            self.subscribe_socket(list(self.coins.keys()))
            self.listen()
        else:
            pid = os.getpid()
            os.system("kill -9 {}".format(pid))
            os.system("Taskkill /PID {} /F".format(pid))

    def subscribe_socket(self, pairs):
        """Method ment to build a subscribe fully to a list of pairs."""
        pass

    def close_connection(self):
        """
        Closes connection to the server
        """
        self.go_on = False
        self._ws.close()