from setuptools import setup, find_packages

setup(
    name='pycryptosockets',
    version='0.2.12',
    description='The unified cryptocurrencies websockets module. Huobipro,\
    Bitbank and other improvements added.',
    long_description='This project is ment to help all the cryptocurrencies\
    developers to get live data from the differents exchanges\
    in one simple place.',
    url='https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets',
    download_url='https://gitlab.com/avanti-sf/public/proyectos-internos/pycryptosockets.git',
    author='Avanti Finantial Services',
    author_email='ph@bitadata.com',
    licence='',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Financial and Insurance Industry',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
    ],
    keywords='exchanges websockets crypto',
    packages=['pycryptosockets', 'pycryptosockets.utils',
              'pycryptosockets.datafeeders'],
    install_requires=['websocket-client', 'python-binance',
                      'bittrex-websocket', 'pubnub',
                      'gevent', 'pyopenssl',
                      'urllib3==1.21.1', 'pyasn1',
                      'requests==2.20', 'six',
                      ]
)
