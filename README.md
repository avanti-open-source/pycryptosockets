# pycryptosockets
[![pipeline status](https://gitlab.com/avanti-sf/public/avanti-team/proyectos-internos/pycryptosockets/badges/master/pipeline.svg)](https://gitlab.com/avanti-sf/public/avanti-team/proyectos-internos/pycryptosockets/-/commits/master)
[![coverage report](https://gitlab.com/avanti-sf/public/avanti-team/proyectos-internos/pycryptosockets/badges/master/coverage.svg)](https://gitlab.com/avanti-sf/public/avanti-team/proyectos-internos/pycryptosockets/-/commits/master)


Developed by Avanti Financial Services.
This project is meant to help all the cryptocurrencies developers to get live trade data from the differents exchanges in one simple place.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Make sure to run (only debian distributions, search for equivalents)
```
sudo apt-get install build-essential libssl-dev libffi-dev python-dev
```
or for other distributions as SLES and derivates
```
sudo zypper in libssl45 python3-devel libffi-devel libffi7
```
before installing

### Installing

The instalation is pretty simple:

```
pip install git+https://gitlab.com/avantidigital/pycryptosockets.git
```

or

```
pip3 install git+https://gitlab.com/avantidigital/pycryptosockets.git
```
Here you're good to go!

## Content

We use the Publisher-Subscriber pattern to deliver data to only one class, with only one interface so the live data is *easy to visualize, easy to handle*.

### Unified interface

In Pycrypto we desing a unified interface for all exchanges.
We have the format COIN-MARKET (ej. BTC-USD, ETH-BTC) as the only accepted pair format. Internally, we manipulate them so we can subscribe to the data correctly for every exchange.

### Deliver Interface

As everything here, we deliver the data in one only format that is very friendly:
ej.
```json
{
    "event": "Update",
    "data": {
                "symbol": "BTC-USD",
                "price": 9100,
                "volume": 0.521,
                "side": "BUY",
                "time": "2018-04-04 11:15:00.739685+00:00"
            },
    "exchange": "BITSTAMP"
}
```

There is also a "event" : "Heartbeat", that's meant to keep the user informed that the connection is still alive.
This doesn't apply to all the data feeders.

## Classes

The code is divided in the following way. We have one primary class to handle the exchange clients, one to handle the coins format, one *base* class thats generalizes the managment of a websocket connection and one class for each websocket client.

### Clients Manager
```python
pycryptosockets.ClientsManager(name='ClientsManager', 
                               exchanges=None, pairs=None,
                               config=None, 
                               auth=None, 
                               usd_equal_usdt=False, 
                               reconnect_on_error=True)

    :param name: Name of the subscriber<br>
    :param exchanges: list of the exchanges<br>
    :param pairs: list of pairs to subscribe<br>
    :param config: custom dictionary for detailed subscriptions<br>
    :param auth: dictionary with the autentication for the exchanges that require it
    :param usd_equal_usd: boolean, tells if Tether will be used as equivalent for USD
    :param reconnect_on_error: boolean, tells if the clients will reconnect after an error. If false all the Manager will close on error.
```
The user will need to provide either *exchange* and *pairs* or a *config*. The latter is a dictionary where the keys are exchange names, and its values are list of coins.
After initializing the class all the user needs to do is to call pycryptosockets.ClientsManager.start()

### Coins Manager

Utility class to handle the coins format

### Base Websocket Client

Model of a Websocket clients with generalized functions and params.


## Authentication

Some exchanges need authentication to work, for them we have the *auth* param in **ClientsManager**.
Here we explain the format and indicate the keys for each exchange:

```json
auth = {
    'Binance': {
        'api_key': '',
        'api_secret': ''
    },
    'Bitbank': {
        'subscribe_key': ''
    }
}
```
## Deployment

For deployment we recommend to set the reconnect_on_error param to *false*, that way you allow your own monitoring system (like Docker, PM2 or even systemd) to keep your processes running more cleanly, because websocket connections can sometimes hang after a reconnect.

## Authors

- Paúl Herrera
- Arnaldo Varela
- Elias Hidalgo
- Freddy Manrique
- Jesús Valdez

## License

This project is pending for license
